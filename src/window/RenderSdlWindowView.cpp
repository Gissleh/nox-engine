/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/window/RenderSdlWindowView.h>

#include <nox/app/IContext.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>
#include <nox/logic/IContext.h>
#include <nox/logic/graphics/event/DebugGeometryChange.h>

#include <assert.h>

namespace nox
{
namespace window
{

RenderSdlWindowView::RenderSdlWindowView(app::IContext* applicationContext, const std::string& windowTitle):
	SdlWindowView(applicationContext, windowTitle, true),
	applicationContext(applicationContext),
	logicContext(nullptr),
	window(nullptr),
	listener("RenderSdlWindowView")
{
	assert(applicationContext != nullptr);

	this->log = applicationContext->createLogger();
	this->log.setName("SdlRendererWindowView");

	this->listener.addEventTypeToListenFor(logic::graphics::DebugGeometryChange::ID);
}

RenderSdlWindowView::~RenderSdlWindowView() = default;

bool RenderSdlWindowView::initialize(logic::IContext* context)
{
	this->logicContext = context;

	if (this->SdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), logic::event::ListenerManager::StartListening_t());

	return true;
}

void RenderSdlWindowView::render()
{
	assert(this->renderer != nullptr);

	this->renderer->onRender();
	SDL_GL_SwapWindow(this->window);
}

bool RenderSdlWindowView::onWindowCreated(SDL_Window* window)
{
	this->window = window;

	this->renderer = std::unique_ptr<app::graphics::OpenGlRenderer>(new app::graphics::OpenGlRenderer());
	std::string shaderDirectory = "nox/shader/";

	if (this->renderer->init(this->applicationContext, shaderDirectory, this->getWindowSize()) == false)
	{
		this->log.fatal().raw("Failed to create OpenGL renderer.");
		return false;
	}
	else
	{
		this->log.verbose().raw("Created OpenGL renderer.");

		this->onRendererCreated(this->renderer.get());

		return true;
	}
}

void RenderSdlWindowView::onSdlEvent(const SDL_Event& event)
{
	this->SdlWindowView::onSdlEvent(event);
}

void RenderSdlWindowView::onDestroy()
{
	this->listener.stopListening();
}

void RenderSdlWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	assert(this->renderer != nullptr);

	this->renderer->resizeWindow(size);
}

void RenderSdlWindowView::onEvent(const std::shared_ptr<logic::event::Event>& event)
{
	if (event->isType(logic::graphics::DebugGeometryChange::ID))
	{
		auto geometryEvent = static_cast<logic::graphics::DebugGeometryChange*>(event.get());

		if (geometryEvent->wasAdded())
		{
			this->renderer->addDebugGeometrySet(geometryEvent->getGeometry());
		}
		else if (geometryEvent->wasRemoved())
		{
			this->renderer->removeDebugGeometrySet(geometryEvent->getGeometry());
		}
	}
}

logic::IContext* RenderSdlWindowView::getLogicContext()
{
	return this->logicContext;
}

app::IContext* RenderSdlWindowView::getApplicationContext()
{
	return this->applicationContext;
}

}
}
