/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Process.h"

namespace nox
{

namespace process
{

Process::Process(std::unique_ptr<Implementation> implementation):
	state(State::UNINITIALIZED),
	implementation(std::move(implementation))
{
	this->implementation->onAttachToProcess(this);
}

Process::~Process()
{
}

void Process::init()
{
	this->state = State::RUNNING;
	this->implementation->onInit();
}

void Process::update(const Duration& deltaTime)
{
	this->implementation->onUpdate(deltaTime);
}

void Process::onSucceed()
{
	this->implementation->onSuccess();
}

void Process::onFail()
{
	this->implementation->onFail();
}

void Process::onAbort()
{
	this->implementation->onAbort();
}

void Process::onUpdateFinished(const float alpha)
{
	this->implementation->onUpdateFinished(alpha);
}

void Process::pause()
{
	this->state = State::PAUSED;
}

void Process::resume()
{
	this->state = State::RUNNING;
}

void Process::failExecution()
{
	this->state = State::FAILED;
}

void Process::succeedExecution()
{
	this->state = State::SUCCEEDED;
}

void Process::abortExecution()
{
	this->state = State::ABORTED;
}

bool Process::isInitialized() const
{
	return (this->state != State::UNINITIALIZED);
}

bool Process::isRunning() const
{
	return (this->state == State::RUNNING);
}

bool Process::isAlive() const
{
	return (this->state == State::RUNNING
			|| this->state == State::PAUSED
			|| this->state == State::UNINITIALIZED);
}

bool Process::didSucceed() const
{
	return (this->state == State::SUCCEEDED);
}

bool Process::didFail() const
{
	return (this->state == State::FAILED);
}

void Process::setSuccessorProcess(std::unique_ptr<Process> successor)
{
	this->successorProcess = std::move(successor);
}

std::unique_ptr<Process> Process::getSuccessor()
{
	return std::move(this->successorProcess);
}

bool Process::wasAborted() const
{
	return (this->state == State::ABORTED);
}

bool Process::hasSuccessor() const
{
	if (this->successorProcess)
	{
		return true;
	}
	else
	{
		return false;
	}
}

}
}
