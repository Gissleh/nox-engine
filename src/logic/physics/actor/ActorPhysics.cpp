/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/actor/ActorPhysics.h>

#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/actor/event/TransformChange.h>
#include <nox/util/json_utils.h>
#include <nox/util/math/angle.h>
#include <boost/geometry/strategies/strategies.hpp>
#include <boost/geometry/algorithms/area.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <nox/logic/physics/Simulation.h>

namespace nox { namespace logic { namespace physics
{

const ActorPhysics::IdType ActorPhysics::NAME = "Physics";

const ActorPhysics::IdType& ActorPhysics::getName() const
{
	return ActorPhysics::NAME;
}

bool ActorPhysics::initialize(const Json::Value& componentJsonObject)
{
	this->physicalProperties.linearVelocity = util::parseJsonVec(componentJsonObject["linearVelocity"], glm::vec2(0.0f));
	this->physicalProperties.angularVelocity = componentJsonObject.get("angularVelocity", 0.0f).asFloat();

	this->physicalProperties.angularDamping = componentJsonObject.get("angularDamping", 0.0f).asFloat();
	this->physicalProperties.linearDamping = componentJsonObject.get("linearDamping", 0.0f).asFloat();
	this->physicalProperties.density = componentJsonObject.get("density", 1.0f).asFloat();
	this->physicalProperties.friction = componentJsonObject.get("friction", 0.2f).asFloat();
	this->physicalProperties.restitution = componentJsonObject.get("restitution", 0.0f).asFloat();

	this->physicalProperties.depth = componentJsonObject.get("depth", 4).asUInt();
	this->physicalProperties.fixedRotation = componentJsonObject.get("fixedRotation", false).asBool();

	this->physicalProperties.bodyType = this->readBodyTypeFromJson(componentJsonObject);
	this->physicalProperties.sensor = componentJsonObject.get("sensor", false).asBool();
	this->physicalProperties.bullet = componentJsonObject.get("bullet", false).asBool();
	this->stickiness = componentJsonObject.get("stickiness", 0.0f).asFloat();


	this->physicalProperties.gravityMultiplier = componentJsonObject.get("gravityMultiplier", 1.0f).asFloat();

	this->physicalProperties.active = false;

	this->physicalProperties.collisionCategory = componentJsonObject.get("collisionCategory", "").asString();

	const Json::Value& collisionMask = componentJsonObject["collisionMask"];
	if (collisionMask != Json::nullValue)
	{
		const Json::Value& exclude = collisionMask["exclude"];

		for (const auto& mask : exclude)
		{
			const auto maskName = mask.asString();
			this->physicalProperties.maskCategories.push_back(maskName);
		}
	}

	this->rotateTowardsPull = componentJsonObject.get("rotateTowardsPull", false).asBool();

	this->physics = this->getLogicContext()->getPhysics();

	if (this->readShapeFromJson(componentJsonObject) == false)
	{
		return false;
	}

	this->originalShape = this->physicalProperties.shape;

	this->shapeScale = util::parseJsonVec(componentJsonObject["shapeScale"], glm::vec2(1.0f, 1.0f));
	this->actorScale = glm::vec2(1.0f, 1.0f);

	return true;
}

void ActorPhysics::updateScaledShape()
{
	const glm::vec2 scale = this->shapeScale * this->actorScale;

	if (this->originalShape.type == ShapeType::POLYGON)
	{
		for (unsigned int i = 0; i < this->originalShape.polygons.size(); i++)
		{
			this->physicalProperties.shape.polygons[i] = util::transformPolygon(this->originalShape.polygons[i], glm::vec2(0.0f, 0.0f), 0.0f, scale);
		}
	}
	else if (this->originalShape.type == ShapeType::CIRCLE)
	{
		this->physicalProperties.shape.circleRadius = this->originalShape.circleRadius * scale.x;
	}
	else if (this->originalShape.type == ShapeType::RECTANGLE)
	{
		this->physicalProperties.shape.box.upperBound = this->originalShape.box.upperBound * scale;
		this->physicalProperties.shape.box.lowerBound = this->originalShape.box.lowerBound * scale;
	}
}

PhysicalBodyType ActorPhysics::readBodyTypeFromJson(const Json::Value& componentJsonObject)
{
	const std::string& bodyTypeValue = componentJsonObject.get("bodyType", "dynamic").asString();

	if (bodyTypeValue == "dynamic")
	{
		return PhysicalBodyType::DYNAMIC;
	}

	if (bodyTypeValue == "static")
	{
		return PhysicalBodyType::STATIC;
	}

	if (bodyTypeValue == "kinematic")
	{
		return PhysicalBodyType::KINEMATIC;
	}

	return PhysicalBodyType::DYNAMIC;
}

glm::vec2 ActorPhysics::getCenterOfMass() const
{
	return this->physics->getActorCenterOfMass(this->getOwner()->getId());
}

bool ActorPhysics::readShapeFromJson(const Json::Value& componentJsonObject)
{
	const Json::Value& shapeObject = componentJsonObject["shape"];

	if (shapeObject.isNull() == false)
	{
		return parseBodyShapeJson(shapeObject, this->physicalProperties.shape);
	}

	return true;
}

void ActorPhysics::onCreate()
{
	auto transformComp = this->getOwner()->findComponent<actor::Transform>();
	if (transformComp != nullptr)
	{
		glm::vec2 position = transformComp->getPosition();
		float rotation = transformComp->getRotation();

		this->physicalProperties.position = position;
		this->physicalProperties.angle = rotation;
	}

	this->updateScaledShape();

	this->physics->createActorBody(this->getOwner(), this->physicalProperties);

	if (this->stickiness > 0.0f)
	{
		this->physics->setSticky(this->getOwner()->getId(), this->stickiness);
	}

	const std::vector<actor::Actor::Child> childActors = this->getOwner()->findChildrenRecursively();

	for (const auto& childActor : childActors)
	{
		this->getLogicContext()->getPhysics()->setActorFriendGroup(childActor.actor->getId(), this->getOwner()->getId());
	}
}

void ActorPhysics::onDestroy()
{
	this->physicalProperties.angularVelocity = this->physics->getAngularVelocity(this->getOwner()->getId());
	this->physicalProperties.linearVelocity = glm::vec2(this->physics->getVelocity(this->getOwner()->getId()));

	this->physics->removeActorBody(this->getOwner()->getId());
}

void ActorPhysics::onUpdate(const Duration& /*deltaTime*/)
{
	if (this->rotateTowardsPull == true)
	{
		float horizontalRotation = this->getHorizonAngle();
		this->setRotation(horizontalRotation);
	}
}

void ActorPhysics::onComponentEvent(const std::shared_ptr<event::Event> &event)
{
	if (event->getType() == actor::TransformChange::ID)
	{
		const auto transformEvent = static_cast<actor::TransformChange*>(event.get());

		if (this->physicalProperties.shape.type != ShapeType::NONE)
		{
			const glm::vec2 transformScale = transformEvent->getScale();

			if (transformScale != this->actorScale)
			{
				this->actorScale = transformScale;

				this->updateScaledShape();

				this->physics->setActorBodyShape(this->getOwner()->getId(), this->physicalProperties.shape);

			}
		}
	}
}

void ActorPhysics::onActivate()
{
	this->physics->activateActorBody(this->getOwner()->getId());
}

void ActorPhysics::onDeactivate()
{
	this->physics->deactivateActorBody(this->getOwner()->getId());
}

void ActorPhysics::applyForce(const glm::vec2& force)
{
	this->physics->applyForce(this->getOwner()->getId(), force);
}

void ActorPhysics::applyForceAtWorldPosition(const glm::vec2& force, const glm::vec2& worldPosition)
{
	this->physics->applyForce(this->getOwner()->getId(), force, worldPosition);
}

void ActorPhysics::applyForceAtBodyPosition(const glm::vec2& force, const glm::vec2& position)
{
	const glm::vec2 forcePosition = this->getPosition() + glm::rotate(position, this->getRotation());
	this->physics->applyForce(this->getOwner()->getId(), force, forcePosition);
}

void ActorPhysics::applyTorque(const float torque)
{
	this->physics->applyTorque(this->getOwner()->getId(), torque);
}
void ActorPhysics::applyImpulse(const glm::vec2& impulse)
{
	this->physics->applyImpulse(this->getOwner()->getId(), impulse);
}

void ActorPhysics::setPosition(const glm::vec2 &position)
{
	this->physics->setActorPosition(this->getOwner()->getId(), position);
}

void ActorPhysics::setVelocity(const glm::vec2& velocity)
{
	this->physicalProperties.linearVelocity = velocity;
	this->physics->setActorVelocity(this->getOwner()->getId(), velocity);
}

void ActorPhysics::setSensor(bool state)
{
	this->physicalProperties.sensor = state;
	this->physics->setSensor(this->getOwner()->getId(), state);
}

void ActorPhysics::setGravityMultiplier(const float multiplier)
{
	this->physicalProperties.gravityMultiplier = multiplier;
	this->physics->setActorGravityMultiplier(this->getOwner()->getId(), multiplier);
}

void ActorPhysics::setRestitution(const float restitution)
{
	this->physicalProperties.restitution = restitution;
	this->physics->setRestitution(this->getOwner()->getId(), restitution);
}

float ActorPhysics::setLinearDamping(const float dampening)
{
	const float previous = this->physicalProperties.linearDamping;

	this->physicalProperties.linearDamping = dampening;
	this->physics->setActorLinearDampening(this->getOwner()->getId(), dampening);

	return previous;
}

void ActorPhysics::setAngularDamping(const float dampening)
{
	this->physicalProperties.angularDamping = dampening;
	this->physics->setActorAngularDampening(this->getOwner()->getId(), dampening);
}

void ActorPhysics::setRotation(const float rotation)
{
	this->physics->setActorRotation(this->getOwner()->getId(), rotation);
}

void ActorPhysics::setCollisionCategory(const std::string & collisionCategory)
{
	this->physics->setCollisionCategory(this->getOwner(), collisionCategory);
}

void ActorPhysics::addCollisionExclusions(const std::vector<std::string>& excludedCategories)
{
	this->physics->addCollisionExclusions(this->getOwner(), excludedCategories);
}

void ActorPhysics::removeCollisionExclusions(const std::vector<std::string>& excludedCategories)
{
	this->physics->removeCollisionExclusions(this->getOwner(), excludedCategories);
}

glm::vec2 ActorPhysics::getVelocity() const
{
	return this->physics->getVelocity(this->getOwner()->getId());
}

glm::vec2 ActorPhysics::getPosition() const
{
	return this->physics->getPostition(this->getOwner()->getId());
}

float ActorPhysics::getRotation() const
{
	return this->physics->getRotation(this->getOwner()->getId());
}

float ActorPhysics::getMass() const
{
	return this->physics->getMassOfActor(this->getOwner()->getId());
}

void ActorPhysics::setAwakeState(bool state)
{
	this->physics->setActorAwakeState(this->getOwner()->getId(), state);
}

void ActorPhysics::serialize(Json::Value& componentObject)
{
	componentObject["angularDamping"] = this->physicalProperties.angularDamping;
	componentObject["linearDamping"] = this->physicalProperties.linearDamping;
	componentObject["density"] = this->physicalProperties.density;
	componentObject["fixedRotation"] = this->physicalProperties.fixedRotation;
	componentObject["depth"] = this->physicalProperties.depth;
	componentObject["friction"] = this->physicalProperties.friction;
	componentObject["restitution"] = this->physicalProperties.restitution;
	componentObject["bullet"] = this->physicalProperties.bullet;
	componentObject["gravityMultiplier"] = this->physicalProperties.gravityMultiplier;
	componentObject["stickiness"] = this->stickiness;
	componentObject["shapeScale"] = util::writeJsonVec(this->shapeScale);
	componentObject["collisionCategory"] = this->physicalProperties.collisionCategory;

	componentObject["collisionMask"] = Json::objectValue;
	Json::Value& collisionMaskValue = componentObject["collisionMask"];

	for (const auto& mask : this->physicalProperties.maskCategories)
	{
		collisionMaskValue["exclude"].append(mask);
	}

	glm::vec2 linearVelocity = this->physicalProperties.linearVelocity;
	float angularVelocity = this->physicalProperties.angularVelocity;

	if (this->getOwner()->isCreated() == true)
	{
		linearVelocity = glm::vec2(this->physics->getVelocity(this->getOwner()->getId()));
		angularVelocity = this->physics->getAngularVelocity(this->getOwner()->getId());
	}

	componentObject["linearVelocity"] = util::writeJsonVec(linearVelocity);
	componentObject["angularVelocity"] = angularVelocity;

	std::string bodyTypeValue;

	if (this->physicalProperties.bodyType == PhysicalBodyType::DYNAMIC)
	{
		bodyTypeValue = "dynamic";
	}
	else if (this->physicalProperties.bodyType == PhysicalBodyType::STATIC)
	{
		bodyTypeValue = "static";
	}
	else if (this->physicalProperties.bodyType == PhysicalBodyType::KINEMATIC)
	{
		bodyTypeValue = "kinematic";
	}

	componentObject["bodyType"] = bodyTypeValue;

	componentObject["shape"] = Json::objectValue;
	Json::Value& shapeObject = componentObject["shape"];

	if (this->originalShape.type == ShapeType::CIRCLE)
	{
		shapeObject["type"] = "circle";
		shapeObject["radius"] = this->originalShape.circleRadius;
	}
	else if (this->originalShape.type == ShapeType::POLYGON)
	{
		shapeObject["type"] = "polygon";
		shapeObject["vertexList"] = Json::arrayValue;
		Json::Value& vertexList = shapeObject["vertexList"];

		//TODO: Write inner polygons (holes).

		const util::BoostPolygon& polygon = this->originalShape.polygons.front();

		for (unsigned int i = 0; i < polygon.outer().size(); i++)
		{
			vertexList[i] = Json::objectValue;
			vertexList[i]["x"] = polygon.outer()[i].x;
			vertexList[i]["y"] = polygon.outer()[i].y;
		}
	}
	else if (this->originalShape.type == ShapeType::RECTANGLE)
	{
		shapeObject["type"] = "box";
		shapeObject["lowerBound"] = util::writeJsonVec(this->originalShape.box.lowerBound);
		shapeObject["upperBound"] = util::writeJsonVec(this->originalShape.box.upperBound);
	}
}

float ActorPhysics::getBodyShapeArea() const
{
	if (this->physicalProperties.shape.type == ShapeType::POLYGON)
	{
		float area = 0.0f;

		for (auto polygon : this->physicalProperties.shape.polygons)
		{
			area += static_cast<float>(boost::geometry::area(polygon));
		}

		return area;
	}
	else if (this->physicalProperties.shape.type == ShapeType::CIRCLE)
	{
		return glm::pi<float>() * (this->physicalProperties.shape.circleRadius *
			       this->physicalProperties.shape.circleRadius);
	}
	else if (this->physicalProperties.shape.type == ShapeType::RECTANGLE)
	{
		glm::vec2 bottomLeft = this->physicalProperties.shape.box.getLowerLeft();
		glm::vec2 topRight = this->physicalProperties.shape.box.getUpperRight();
		if (bottomLeft.x < topRight.x && bottomLeft.y < topRight.y)
		{
			return this->physicalProperties.shape.box.getHeight() * this->physicalProperties.shape.box.getWidth();
		}
		else
		{
			return 0.0f;
		}
	}
	else
	{
		return 0.0f;
	}
}

void ActorPhysics::setFriendActorGroup(const actor::Identifier& friendlyId)
{
	this->physicalProperties.friendActorId = friendlyId;
	this->physics->setActorFriendGroup(this->getOwner()->getId(), friendlyId);
}

actor::Identifier ActorPhysics::getFriendActorGroup() const
{
	return this->physicalProperties.friendActorId;
}

const glm::vec2 ActorPhysics::getGravitationalPull() const
{
	return this->physics->getGravitationalPullOnActor(this->getOwner()->getId());
}

float ActorPhysics::getGravitationalPullAngle() const
{
	const glm::vec2 pull = this->getGravitationalPull();

	return glm::atan(pull.y, pull.x);
}

float ActorPhysics::getHorizonAngle() const
{
	return this->getGravitationalPullAngle() + math::quarterCircle<float>();
}

void ActorPhysics::setFixedRotation(const bool fixed)
{
	this->physicalProperties.fixedRotation = fixed;
	this->physics->setFixedRotation(this->getOwner()->getId(), fixed);
}

bool ActorPhysics::hasFixedRotation() const
{
	return this->physics->hasFixedRotation(this->getOwner()->getId());
}

bool ActorPhysics::bodyAabbIntersectsAabb(const math::Box<glm::vec2>& aabb) const
{
	return this->physics->actorAabbIntersectsAabb(this->getOwner()->getId(), aabb);
}

float ActorPhysics::getMassOfStrongestGravityPullBody() const
{
	return this->physics->getMassOfStrongestGravityPullBody(this->getOwner()->getId());
}

float ActorPhysics::getDistanceOfStrongestGravityPullBody() const
{
	return this->physics->getDistanceToStrongestGravityPullBody(this->getOwner()->getId());
}

float ActorPhysics::getBodyRadius() const
{
	const BodyShape& shape = this->physicalProperties.shape;

	if (shape.type == ShapeType::CIRCLE)
	{
		return shape.circleRadius;
	}
	else if (shape.type == ShapeType::POLYGON)
	{
		float furthestDistance = 0.0f;

		for (const util::BoostPolygon& polygon : shape.polygons)
		{
			for (const glm::vec2& point : polygon.outer())
			{
				furthestDistance = glm::max(furthestDistance, glm::length(point));
			}
		}

		return furthestDistance;
	}
	else
	{
		return 0.0f;
	}
}

bool ActorPhysics::isSensor() const
{
	return this->physicalProperties.sensor;
}

float ActorPhysics::getGravityMultiplier() const
{
	return this->physicalProperties.gravityMultiplier;
}

void ActorPhysics::setBodyShape(const BodyShape& shape)
{
	this->originalShape = shape;
	this->physicalProperties.shape.position = shape.position;
	this->physicalProperties.shape.type = shape.type;
	this->physicalProperties.shape.polygons.resize(shape.polygons.size());
	this->physicalProperties.shape.circleRadius = shape.circleRadius;

	this->updateScaledShape();

	this->physics->setActorBodyShape(this->getOwner()->getId(), this->physicalProperties.shape);

	if (this->physicalProperties.sensor == true)
	{
		this->physics->setSensor(this->getOwner()->getId(), true);
	}
}

bool ActorPhysics::isDynamic() const
{
	return this->physicalProperties.bodyType == PhysicalBodyType::DYNAMIC;
}

bool ActorPhysics::isStatic() const
{
	return this->physicalProperties.bodyType == PhysicalBodyType::STATIC;
}

bool ActorPhysics::isKinematic() const
{
	return this->physicalProperties.bodyType == PhysicalBodyType::KINEMATIC;
}

float ActorPhysics::getAngularVelocity() const
{
	return this->physics->getAngularVelocity(this->getOwner()->getId());
}


} } }
