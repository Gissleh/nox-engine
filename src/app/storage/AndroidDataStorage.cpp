/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/storage/AndroidDataStorage.h>

#include <nox/app/log/Logger.h>
#include <nox/util/android/FileInputStream.h>
#include <nox/util/android/FileOutputStream.h>

namespace nox { namespace app { namespace storage {

class AndroidDataStorage::Impl
{
public:
	Impl(JNIEnv* javaEnv, jobject javaAndroidContext);

	bool fileExists(jobject& fileJava);
	jobject openFile(const std::string& path);
	std::string getResidingDirectoryPath(const std::string& path);

	log::Logger log;

	std::string rootPath;

	JNIEnv* javaEnv;
	jobject javaAndroidContext;

	jclass contextClass;
	jmethodID getDirMethodId;

	jclass fileClass;
	jmethodID fileConstructorPathId;
	jmethodID existsMethodId;
	jmethodID deleteMethodId;
	jmethodID mkdirsMethodId;

	jclass fileOutputStreamClass;
	jmethodID fileOutputStreamConstructorFileId;

	jclass fileInputStreamClass;
	jmethodID fileInputStreamConstructorFileId;
};

AndroidDataStorage::Impl::Impl(JNIEnv* javaEnv, jobject javaAndroidContext):
	javaEnv(javaEnv),
	javaAndroidContext(javaAndroidContext)
{
	this->contextClass = this->javaEnv->GetObjectClass(this->javaAndroidContext);
	this->getDirMethodId = this->javaEnv->GetMethodID(this->contextClass, "getDir", "(Ljava/lang/String;I)Ljava/io/File;");

	this->fileClass = javaEnv->FindClass("java/io/File");
	this->fileConstructorPathId = this->javaEnv->GetMethodID(this->fileClass, "<init>", "(Ljava/lang/String;)V");
	this->existsMethodId = this->javaEnv->GetMethodID(this->fileClass, "exists", "()Z");
	this->deleteMethodId = this->javaEnv->GetMethodID(this->fileClass, "delete", "()Z");
	this->mkdirsMethodId = this->javaEnv->GetMethodID(this->fileClass, "mkdirs", "()Z");

	this->fileInputStreamClass = javaEnv->FindClass("java/io/FileInputStream");
	this->fileInputStreamConstructorFileId = this->javaEnv->GetMethodID(this->fileInputStreamClass, "<init>", "(Ljava/io/File;)V");
	this->fileOutputStreamClass = javaEnv->FindClass("java/io/FileOutputStream");
	this->fileOutputStreamConstructorFileId = this->javaEnv->GetMethodID(this->fileOutputStreamClass, "<init>", "(Ljava/io/File;Z)V");

	const auto getFilesDirMethodId = this->javaEnv->GetMethodID(this->contextClass, "getFilesDir", "()Ljava/io/File;");
	//const auto getExternalFilesDirMethodId = this->javaEnv->GetMethodID(this->contextClass, "getExternalFilesDir", "(Ljava/lang/String;)Ljava/io/File;");
	const auto getPathMethodId = this->javaEnv->GetMethodID(this->fileClass, "getPath", "()Ljava/lang/String;");

	auto filesDirFileJava = this->javaEnv->CallObjectMethod(this->javaAndroidContext, getFilesDirMethodId);
	//auto filesDirFileJava = this->javaEnv->CallObjectMethod(this->javaAndroidContext, getExternalFilesDirMethodId, nullptr);
	auto filesDirPathJava = static_cast<jstring>(this->javaEnv->CallObjectMethod(filesDirFileJava, getPathMethodId));
	this->javaEnv->DeleteLocalRef(filesDirFileJava);

	auto filesDirPathCStr = this->javaEnv->GetStringUTFChars(filesDirPathJava, nullptr);
	auto filesDirPath = std::string(filesDirPathCStr);
	this->javaEnv->ReleaseStringUTFChars(filesDirPathJava, filesDirPathCStr);
	this->javaEnv->DeleteLocalRef(filesDirPathJava);

	this->rootPath = filesDirPath + "/";
}

jobject AndroidDataStorage::Impl::openFile(const std::string& path)
{
	auto pathJavaString = this->javaEnv->NewStringUTF(path.c_str());
	auto fileJava = this->javaEnv->NewObject(this->fileClass, this->fileConstructorPathId, pathJavaString);
	this->javaEnv->DeleteLocalRef(pathJavaString);

	return fileJava;
}

bool AndroidDataStorage::Impl::fileExists(jobject& fileJava)
{
	auto existsJava = this->javaEnv->CallBooleanMethod(fileJava, this->existsMethodId);

	auto exists = false;
	if (existsJava == JNI_TRUE)
	{
		exists = true;
	}

	return exists;
}

std::string AndroidDataStorage::Impl::getResidingDirectoryPath(const std::string& path)
{
	const auto lastDelimiterPos = path.find_last_of('/');
	if (lastDelimiterPos == std::string::npos || lastDelimiterPos == 0)
	{
		return ".";
	}
	else
	{
		return path.substr(0, lastDelimiterPos);
	}
}

AndroidDataStorage::AndroidDataStorage(JNIEnv* javaEnv, jobject javaAndroidContext):
	d(std::make_unique<Impl>(javaEnv, javaAndroidContext))
{
}

AndroidDataStorage::~AndroidDataStorage() = default;

void AndroidDataStorage::setLogger(log::Logger logger)
{
	this->d->log = std::move(logger);
	this->d->log.setName("AndroidDataStorage");
}

bool AndroidDataStorage::fileExists(const std::string& filePath) const
{
	const auto fullPath = this->d->rootPath + filePath;

	auto fileJava = this->d->openFile(fullPath);
	const auto exists = this->d->fileExists(fileJava);

	this->d->javaEnv->DeleteLocalRef(fileJava);

	return exists;
}

void AndroidDataStorage::removeFile(const std::string& filePath)
{
	const auto fullPath = this->d->rootPath + filePath;

	auto fileJava = this->d->openFile(fullPath);
	this->d->javaEnv->CallBooleanMethod(fileJava, this->d->deleteMethodId);
	this->d->javaEnv->DeleteLocalRef(fileJava);
}

std::unique_ptr<std::istream> AndroidDataStorage::openReadableFile(const std::string& filePath)
{
	auto inputStream = std::unique_ptr<std::istream>();

	const auto fullPath = this->d->rootPath + filePath;

	this->d->log.debug().format("Trying to open file \"%s\" for reading.", fullPath.c_str());

	auto fileJava = this->d->openFile(fullPath);
	if (this->d->fileExists(fileJava) == true)
	{
		this->d->log.debug().format("File exists. Creating input stream.");

		auto inputStreamJava = this->d->javaEnv->NewObject(this->d->fileInputStreamClass, this->d->fileInputStreamConstructorFileId, fileJava);

		inputStream = std::make_unique<android::FileInputStream>(this->d->javaEnv, inputStreamJava);
	}
	else
	{
		this->d->log.error().raw("Failed opening file. Returning nullptr.");
	}

	this->d->javaEnv->DeleteLocalRef(fileJava);

	return inputStream;
}

std::unique_ptr<std::ostream> AndroidDataStorage::openWritableFile(const std::string& filePath, bool append)
{
	auto outputStream = std::unique_ptr<std::ostream>();

	const auto fullPath = this->d->rootPath + filePath;

	this->d->log.debug().format("Trying to open file \"%s\" for writing.", fullPath.c_str());

	auto fileJava = this->d->openFile(fullPath);

	if (this->d->fileExists(fileJava) == false)
	{
		const auto directoryPath = this->d->getResidingDirectoryPath(fullPath);

		this->d->log.debug().format("File does not exist. Creating directories \"%s\".", directoryPath.c_str());

		auto directoryPathJava = this->d->javaEnv->NewStringUTF(directoryPath.c_str());
		auto directoryFileJava = this->d->javaEnv->NewObject(this->d->fileClass, this->d->fileConstructorPathId, directoryPathJava);
		this->d->javaEnv->CallBooleanMethod(directoryFileJava, this->d->mkdirsMethodId);
		//auto directoryFileJava = this->d->javaEnv->CallObjectMethod(this->d->javaAndroidContext, this->d->getDirMethodId, directoryPathJava, 0);
		this->d->javaEnv->DeleteLocalRef(directoryFileJava);
		this->d->javaEnv->DeleteLocalRef(directoryPathJava);
	}

	this->d->log.debug().format("Creating output stream for file.");

	auto appendJava = static_cast<jboolean>(append);
	auto outputStreamJava = this->d->javaEnv->NewObject(this->d->fileOutputStreamClass, this->d->fileOutputStreamConstructorFileId, fileJava, appendJava);

	outputStream = std::make_unique<android::FileOutputStream>(this->d->javaEnv, outputStreamJava);

	this->d->javaEnv->DeleteLocalRef(fileJava);

	return outputStream;
}

std::string AndroidDataStorage::readFileContent(const std::string& filePath)
{
	auto stream = this->openReadableFile(filePath);

	if (stream == nullptr)
	{
		return std::string();
	}
	else
	{
		std::istreambuf_iterator<char> streamStart(*stream);
		std::istreambuf_iterator<char> streamEnd;

		return std::string(streamStart, streamEnd);
	}
}

void AndroidDataStorage::writeFileContent(const std::string& filePath, const std::string& content, bool append)
{
	auto stream = this->openWritableFile(filePath, append);

	*stream << content;
}

} } }


