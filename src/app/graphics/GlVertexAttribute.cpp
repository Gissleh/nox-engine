/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlVertexAttribute.h>

namespace nox { namespace app { namespace graphics {

bool GlVertexAttributeLocation::isUsed() const
{
	return this->used;
}

std::size_t GlVertexAttributeLocation::getLocation() const
{
	return this->index;
}

GlPackedVertexAttributes::GlPackedVertexAttributes(const GLenum bufferUsage, const GlPackedVertexAttributeDef& packDef):
	stride(0)
{
	for (const auto& def : packDef.defList)
	{
		const auto* pointer = reinterpret_cast<const char*>(0) + def.offset;

		if (def.location.isUsed())
		{
			Attribute attrib;
			attrib.index = static_cast<GLuint>(def.location.getLocation());
			attrib.size = static_cast<GLint>(def.numComponents);
			attrib.type = glDataTypeToEnum(def.componentType);
			attrib.normalized = static_cast<GLboolean>(def.normalized);
			attrib.pointer = reinterpret_cast<const GLvoid*>(pointer);

			this->attributes.push_back(attrib);
		}
	}

	if (this->attributes.empty() == true)
	{
		throw std::logic_error("GlPackedVertexAttributes with no locations is not allowed.");
	}

	this->stride = static_cast<GLsizei>(packDef.packSize);

	this->buffer = GlBuffer::generateArrayBuffer(bufferUsage);
}

void GlPackedVertexAttributes::bind(GlContextState* state)
{
	this->buffer.bind(state);

	for (auto& attrib : this->attributes)
	{
		glVertexAttribPointer(attrib.index, attrib.size, attrib.type, attrib.normalized, this->stride, attrib.pointer);
		glEnableVertexAttribArray(attrib.index);
	}
}

GlBuffer& GlPackedVertexAttributes::getBuffer()
{
	return this->buffer;
}

} } }
