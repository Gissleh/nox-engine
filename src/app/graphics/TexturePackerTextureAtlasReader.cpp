/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TexturePackerTextureAtlasReader.h>

namespace nox { namespace app
{
namespace graphics
{

bool TexturePackerTextureAtlasReader::readAtlasJson(const Json::Value& atlasObject, unsigned int atlasId, unsigned int texelsPerMeter)
{
	Json::Value framesArray = atlasObject.get("frames", Json::nullValue);
	if (framesArray.isNull() || framesArray.isArray() == false)
	{
		this->setErrorString("json atlas with no frames array.");
		return false;
	}

	Json::Value atlasMeta = atlasObject.get("meta", Json::nullValue);
	if (atlasMeta.isNull())
	{
		this->setErrorString("json atlas with no meta.");
		return false;
	}

	Json::Value atlasSize = atlasMeta.get("size", Json::nullValue);
	if (atlasSize.isNull())
	{
		this->setErrorString("json atlas with no meta size.");
		return false;
	}

	Json::Value atlasWidth = atlasSize.get("w", Json::nullValue);
	if (atlasWidth.isNull())
	{
		this->setErrorString("json atlas with no width specified.");
		return false;
	}

	Json::Value atlasHeight = atlasSize.get("h", Json::nullValue);
	if (atlasHeight.isNull())
	{
		this->setErrorString("json atlas with no height specified.");
		return false;
	}

	this->atlasWidth = atlasWidth.asUInt();
	this->atlasHeight = atlasHeight.asUInt();

	for (const Json::Value& frameObject : framesArray)
	{
		this->readAtlasFrame(frameObject, atlasId, texelsPerMeter);
	}

	return true;
}

bool TexturePackerTextureAtlasReader::readAtlasFrame(const Json::Value& frameObject, unsigned int atlasId, unsigned int texelsPerMeter)
{
	const auto& frameName = frameObject.get("filename", Json::nullValue);
	if (frameName.isNull())
	{
		this->setErrorString("json atlas frame object without name.");
		return false;
	}

	const auto& frame = frameObject.get("frame", Json::nullValue);
	if (frame.isNull())
	{
		this->setErrorString("json atlas frame object without frame.");
		return false;
	}

	const auto& framePosX = frame.get("x", Json::nullValue);
	if (framePosX.isNull())
	{
		this->setErrorString("json atlas frame without x position.");
		return false;
	}

	const auto& framePosY = frame.get("y", Json::nullValue);
	if (framePosY.isNull())
	{
		this->setErrorString("json atlas frame without y position.");
		return false;
	}

	const auto& frameWidth = frame.get("w", Json::nullValue);
	if (frameWidth.isNull())
	{
		this->setErrorString("json atlas frame without width.");
		return false;
	}

	const auto& frameHeight = frame.get("h", Json::nullValue);
	if (frameHeight.isNull())
	{
		this->setErrorString("json atlas frame without height.");
		return false;
	}

	const auto rotated = frameObject.get("rotated", false).asBool();
	const auto posX = framePosX.asFloat();
	const auto posY = framePosY.asFloat();
	const auto width = frameWidth.asFloat();
	const auto height = frameHeight.asFloat();

	const auto texelScale = 1.0f / static_cast<float>(texelsPerMeter);

	TextureQuad sprite(atlasId);
	TextureQuad::RenderQuad spriteQuad;

	spriteQuad.bottomLeft.setPosition({0.0f, 0.0f});
	spriteQuad.bottomRight.setPosition({width * texelScale, 0.0f});
	spriteQuad.topRight.setPosition({width * texelScale, height * texelScale});
	spriteQuad.topLeft.setPosition({0.0f, height * texelScale});

	const auto atlasWidthScale = 1.0f / static_cast<float>(this->atlasWidth);
	const auto atlasHeightScale = 1.0f / static_cast<float>(this->atlasHeight);

	const auto textureCoordLeft = posX * atlasWidthScale;
	const auto textureCoordTop = posY * atlasHeightScale;

	if (rotated == true)
	{
		/*
		 * PNG is flipped upside down, so we flip the top and bottom y texture values.
		 * We also flip the height with width to handle the 90 degree rotation.
		 * textureCoord* here is relative to the texture.
		 */
		const auto textureCoordRight = (posX + height) * atlasWidthScale;
		const auto textureCoordBottom = (posY + width) * atlasHeightScale;

		spriteQuad.bottomLeft.setTextureCoordinate({textureCoordLeft, textureCoordTop});
		spriteQuad.bottomRight.setTextureCoordinate({textureCoordLeft, textureCoordBottom});
		spriteQuad.topRight.setTextureCoordinate({textureCoordRight, textureCoordBottom});
		spriteQuad.topLeft.setTextureCoordinate({textureCoordRight, textureCoordTop});
	}
	else
	{
		/*
		 * PNG is flipped upside down, so we flip the top and bottom y texture values.
		 */
		const auto textureCoordRight = (posX + width) * atlasWidthScale;
		const auto textureCoordBottom = (posY + height) * atlasHeightScale;

		spriteQuad.bottomLeft.setTextureCoordinate({textureCoordLeft, textureCoordBottom});
		spriteQuad.bottomRight.setTextureCoordinate({textureCoordRight, textureCoordBottom});
		spriteQuad.topRight.setTextureCoordinate({textureCoordRight, textureCoordTop});
		spriteQuad.topLeft.setTextureCoordinate({textureCoordLeft, textureCoordTop});
	}

	sprite.setRenderQuad(spriteQuad);
	this->spriteMap[frameName.asString()] = sprite;

	return true;
}

}
} }
