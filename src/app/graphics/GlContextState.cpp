/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlContextState.h>

#include <cassert>

namespace nox { namespace app { namespace graphics {

GlContextState::GlContextState(GlVersion glVersion):
	currentlyBoundVao(0),
	currentlyBoundShaderProgram(0),
	glVersion(glVersion),
	stencilFailOperation(GL_KEEP),
	stencilMask(0xFF)
{
}

GlContextState::GlContextState():
	GlContextState(GlVersion(0, 0, GlVersion::Type::DESKTOP))
{
}

void GlContextState::setLogger(nox::app::log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("GlContextState");
}

#if NOX_OPENGL_SUPPORT_VAO
GLuint GlContextState::bindVertexArray(GLuint vao)
{
	GLuint previous = this->currentlyBoundVao;

	if (this->currentlyBoundVao != vao)
	{
		glBindVertexArray(vao);
		this->currentlyBoundVao = vao;

		/**
		 * Binding a VAO will also bind its ELEMENT_ARRAY_BUFFER so we have to
		 * properly update the currentlyBound ELEMENT_ARRAY_BUFFER to reflect
		 * this.
		 */
		if (vao == 0)
		{
			// No bound VAO so previous VAO's GL_ELEMENT_ARRAY_BUFFER is unbound.
			const auto elementArrayBuffer = this->vaoBoundElementArray[previous];
			if (elementArrayBuffer != 0)
			{
				this->currentlyBoundVbo[GL_ELEMENT_ARRAY_BUFFER] = 0;
			}
		}
		else
		{
			// New VAO bound so new ELEMENT_ARRAY_BUFFER bound.
			const auto elementArrayBuffer = this->vaoBoundElementArray[this->currentlyBoundVao];
			if (elementArrayBuffer != 0)
			{
				this->currentlyBoundVbo[GL_ELEMENT_ARRAY_BUFFER] = elementArrayBuffer;
			}
		}
	}

#ifndef NDEBUG
	assert(this->currentlyBoundVao == vao);

	auto boundVao = GLint(0);
	glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &boundVao);
	assert(static_cast<GLuint>(boundVao) == vao);

	const auto elementArrayBuffer = this->vaoBoundElementArray[this->currentlyBoundVao];
	if (elementArrayBuffer != 0)
	{
		auto boundVbo = GLint(0);
		glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &boundVbo);
		assert(static_cast<GLuint>(boundVbo) == elementArrayBuffer);
	}
#endif

	return previous;
}
#endif

GLuint GlContextState::bindBuffer(const GLenum target, const GLuint vbo)
{
	const auto previous = this->currentlyBoundVbo[target];

#if NOX_OPENGL_SUPPORT_VAO
	/**
	 * Special care must be applied when we are using VAO's. This is because
	 * GL_ELEMENT_ARRAY_BUFFERs bound while a VAO is bound will be attached to the VAO
	 * and rebound each time the VAO is bound.
	 */
	if (target == GL_ELEMENT_ARRAY_BUFFER)
	{
		/**
		 * If the buffer is attached to a VAO, bind the VAO in stead.
		 * If it isn't and a VAO is currently bound, then the buffer will
		 * be attached to the VAO, so we have to update our state.
		 *
		 * TODO: This will make it impossible to bind one element array buffer to
		 * multiple VAO's.
		 */
		const auto attachedVao = this->elementArrayBoundVao[vbo];
		if (attachedVao != 0)
		{
			this->bindVertexArray(attachedVao);
		}
		else if (this->currentlyBoundVao != 0)
		{
			const auto previousElementArray = this->vaoBoundElementArray[this->currentlyBoundVao];
			if (vbo != previousElementArray)
			{
				this->vaoBoundElementArray[this->currentlyBoundVao] = vbo;
				this->elementArrayBoundVao[vbo] = this->currentlyBoundVao;
				this->currentlyBoundVbo[target] = vbo;
				glBindBuffer(target, vbo);
			}
		}
	}
	else if (vbo != previous)
	{
		glBindBuffer(target, vbo);
		this->currentlyBoundVbo[target] = vbo;
	}
#else
	if (vbo != previous)
	{
		glBindBuffer(target, vbo);
		this->currentlyBoundVbo[target] = vbo;
	}
#endif

#ifndef NDEBUG
	assert(this->currentlyBoundVbo[target] == vbo);

	auto boundVbo = GLint(0);
	if (target == GL_ARRAY_BUFFER)
	{
		glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &boundVbo);
		assert(static_cast<GLuint>(boundVbo) == vbo);
	}
	else if (target == GL_ELEMENT_ARRAY_BUFFER)
	{
		glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &boundVbo);
		assert(static_cast<GLuint>(boundVbo) == vbo);
		if (this->currentlyBoundVao != 0)
		{
			assert(this->vaoBoundElementArray[this->currentlyBoundVao] == vbo);
		}
	}
#endif

	return previous;
}

GLuint GlContextState::bindShaderProgram(GLuint shaderProgram)
{
	GLuint previous = this->currentlyBoundShaderProgram;

	if (this->currentlyBoundShaderProgram != shaderProgram)
	{
		glUseProgram(shaderProgram);
		this->currentlyBoundShaderProgram = shaderProgram;
	}

	return previous;
}

GLuint GlContextState::bindTexture(GLenum target, GLuint texture)
{
	GLuint previous = this->currentlyBoundTexture[target];

	if (this->currentlyBoundTexture[target] != texture)
	{
		glBindTexture(target, texture);
		this->currentlyBoundTexture[target] = texture;
	}

	return previous;
}

void GlContextState::setStencilFailOperation(GLenum operation)
{
	if (operation != this->stencilFailOperation)
	{
		glStencilOp(operation, GL_KEEP, GL_KEEP);
		this->stencilFailOperation = operation;
	}
}

void GlContextState::setStencilMask(GLuint mask)
{
	if (mask != this->stencilMask)
	{
		glStencilMask(mask);
		this->stencilMask = mask;
	}
}

void GlContextState::setStencilFunc(GLenum func, GLint ref, GLuint mask)
{
	StencilFunc otherFunc(func, ref, mask);

	if (otherFunc != this->stencilFunc)
	{
		glStencilFunc(func, ref, mask);
		this->stencilFunc = otherFunc;
	}
}

void GlContextState::enable(GLenum state)
{
	if (this->enabledStates[state] == false)
	{
		glEnable(state);
		this->enabledStates[state] = true;
	}
}

void GlContextState::disable(GLenum state)
{
	if (this->enabledStates[state] == true)
	{
		glDisable(state);
		this->enabledStates[state] = false;
	}
}

GlVersion GlContextState::getGlVersion() const
{
	return this->glVersion;
}

GLuint GlContextState::getBoundVao() const
{
	return this->currentlyBoundVao;
}

GLuint GlContextState::getBoundVbo(GLenum target) const
{
	const auto targetIt = this->currentlyBoundVbo.find(target);

	if (targetIt != this->currentlyBoundVbo.end())
	{
		return targetIt->second;
	}
	else
	{
		return 0u;
	}
}

GLuint GlContextState::getBoundShaderProgram() const
{
	return this->currentlyBoundShaderProgram;
}

GLuint GlContextState::getBoundTexture(GLenum target) const
{
	const auto targetIt = this->currentlyBoundTexture.find(target);

	if (targetIt != this->currentlyBoundTexture.end())
	{
		return targetIt->second;
	}
	else
	{
		return 0u;
	}
}

GlContextState::StencilFunc::StencilFunc():
	func(GL_ALWAYS),
	ref(0),
	mask(0xFF)
{
}

GlContextState::StencilFunc::StencilFunc(GLenum func, GLint ref, GLuint mask):
	func(func),
	ref(ref),
	mask(mask)
{
}

bool GlContextState::StencilFunc::operator!=(const StencilFunc& other) const
{
	return (this->func != other.func || this->ref != other.ref || this->mask != other.mask);
}

}
} }
