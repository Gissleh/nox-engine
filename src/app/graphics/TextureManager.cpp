/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TextureManager.h>
#include <nox/app/graphics/TexturePackerTextureAtlasReader.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/common/platform.h>
#include <nox/app/graphics/opengl_include.h>

#include <fstream>
#include <memory>
#include <json/reader.h>
#include <lodepng.h>

namespace nox { namespace app
{
namespace graphics
{

TextureManager::TextureManager():
	defaultTexelsPerMeter(70)
{
}

void TextureManager::setLogger(log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("TextureManager");
}

bool TextureManager::readGraphicsFile(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess)
{
	auto graphicsJsonResource = resourceAccess->getHandle(graphicsResourceDescriptor);

	if (graphicsJsonResource == nullptr)
	{
		this->log.error().format("Could not find texture atlas: %s", graphicsResourceDescriptor.getPath().c_str());
		return false;
	}

	auto graphicsJsonData = graphicsJsonResource->getExtraData<app::resource::JsonExtraData>();
	assert(graphicsJsonData != nullptr);

	const auto& graphicsJson = graphicsJsonData->getRootValue();

	const unsigned int texelsPerMeter = graphicsJson.get("texelsPerMeter", this->defaultTexelsPerMeter).asUInt();

	const auto& textureAtlasArray = graphicsJson["textureAtlases"];
	if (textureAtlasArray.isNull())
	{
		this->log.error().raw("JSON without texture atlas info.");
		return false;
	}
	else if (textureAtlasArray.isArray() == false)
	{
		this->log.error().raw("JSON atlas array was not an array.");
		return false;
	}

	const auto numAtlases = textureAtlasArray.size();
	this->textureAtlasFiles.reserve(numAtlases);

	for (const auto& atlasObject : textureAtlasArray)
	{
		TextureAtlas atlas;

		const auto& atlasName = atlasObject["name"];
		if (atlasName.isNull())
		{
			this->log.error().raw("JSON atlas without name.");
		}
		else
		{
			atlas.name = atlasName.asString();
			atlas.imageExtension = atlasObject.get("imageExtension", "png").asString();
			atlas.dataExtension = atlasObject.get("dataExtension", "json").asString();
			atlas.minFiltering = atlasObject.get("minFiltering", "linear").asString();
			atlas.magFiltering = atlasObject.get("magFiltering", "linear").asString();
			atlas.mipmapMinFiltering = atlasObject.get("minMipmapFiltering", "linear").asString();
			atlas.mipmapMagFiltering = atlasObject.get("magMipmapFiltering", "linear").asString();
			atlas.boundTextureBuffer = 0;
			atlas.id = static_cast<unsigned int>(this->textureAtlasFiles.size());
			atlas.mipmapMag = atlasObject.get("magMipmap", false).asBool();
			atlas.mipmapMin = atlasObject.get("minMipmap", false).asBool();

			const resource::Descriptor atlasDescriptor(atlas.name + "." + atlas.dataExtension);
			auto atlasJsonResource = resourceAccess->getHandle(atlasDescriptor);

			if (atlasJsonResource == nullptr)
			{
		//		this->log.error().format("Could not find texture atlas: %s", atlasDescriptor.getPath().c_str());
			}
			else
			{
				auto atlasJsonData = atlasJsonResource->getExtraData<app::resource::JsonExtraData>();
				assert(atlasJsonData != nullptr);

				const auto& atlasJson = atlasJsonData->getRootValue();

				TexturePackerTextureAtlasReader atlasReader;

				if (atlasReader.readAtlasJson(atlasJson, atlas.id, texelsPerMeter) == true)
				{
					atlas.width = atlasReader.getAtlasWidth();
					atlas.height = atlasReader.getAtlasHeight();

					this->textureAtlasFiles.push_back(atlas);
					this->textureMap.insert(atlasReader.getTextureMap().begin(), atlasReader.getTextureMap().end());
				}
				else
				{
					this->log.error().format("Could not read atlas file \"%s\": %s", atlasDescriptor.getPath().c_str(), atlasReader.getErrorString().c_str());
				}
			}
		}
	}

	auto defaultTextureName = graphicsJson.get("defaultTexture", std::string()).asString();

	if (defaultTextureName.empty() == false)
	{
		auto textureIt = this->textureMap.find(defaultTextureName);

		if (textureIt != this->textureMap.end())
		{
			this->defaultQuad = textureIt->second;
		}
	}

	return true;
}

void TextureManager::writeToTextureBuffers(GlContextState& glState, resource::IResourceAccess* resourceAccess)
{
	for (auto& atlas : this->textureAtlasFiles)
	{
		if (atlas.boundTextureBuffer == 0)
		{
			atlas.boundTextureBuffer = this->writeToTextureBuffer(glState, atlas, resourceAccess);
		}
	}
}

GLuint TextureManager::writeToTextureBuffer(GlContextState& glState, const TextureAtlas& atlas, resource::IResourceAccess* resourceAccess) const
{
	const auto imageResourceDescriptor = resource::Descriptor(atlas.name + "." + atlas.imageExtension);

	auto imageResource = resourceAccess->getHandle(imageResourceDescriptor);
	if (imageResource == nullptr)
	{
		this->log.error().format("Could not find texture atlas: %s", imageResourceDescriptor.getPath().c_str());
		return 0;
	}

	auto imagePixels = std::vector<unsigned char>{};
	auto imageWidth = 0u;
	auto imageHeight = 0u;
	auto imageState = lodepng::State();
	const auto loadError = lodepng::decode(
		imagePixels,
		imageWidth,
		imageHeight,
		imageState,
		reinterpret_cast<const unsigned char*>(imageResource->getResourceBuffer()),
		imageResource->getResourceBufferSize());

	if (loadError)
	{
		this->log.error().format("Failed loading image \"%s\": %s", imageResourceDescriptor.getPath().c_str(), lodepng_error_text(loadError));
		return 0;
	}

	auto colorMode = GL_RGB;

	if (imageState.info_png.color.colortype == LCT_RGBA)
	{
		this->log.debug().format("Texture \"%s\" has the color type RGBA.", imageResourceDescriptor.getPath().c_str());
		colorMode = GL_RGBA;
	}
	else if (imageState.info_png.color.colortype != LCT_RGB)
	{
		this->log.error().format("Texture \"%s\" has an unsupported color type. Must be RGB or RGBA.", imageResourceDescriptor.getPath().c_str());
		return 0;
	}
	else
	{
		this->log.debug().format("Texture \"%s\" has the color type RGB.", imageResourceDescriptor.getPath().c_str());
	}

	GLuint buffer = 0;

	glGenTextures(1, &buffer);
	glState.bindTexture(GL_TEXTURE_2D, buffer);

	if (atlas.mipmapMag == false)
	{
		if (atlas.magFiltering == "nearest")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
		else if (atlas.magFiltering == "linear")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
		else
		{
			this->log.error().format("Unknown magnify filtering type: %s", atlas.magFiltering.c_str());
		}
	}
	else
	{
		if (atlas.magFiltering == "nearest" && atlas.mipmapMagFiltering == "nearest")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		}
		else if (atlas.magFiltering == "nearest" && atlas.mipmapMagFiltering == "linear")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST_MIPMAP_LINEAR);
		}
		else if (atlas.magFiltering == "linear" && atlas.mipmapMagFiltering == "nearest")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		}
		else if (atlas.magFiltering == "linear" && atlas.mipmapMagFiltering == "linear")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		else
		{
			this->log.error().format("Unknown magnify filtering type: %s + %s mipmap", atlas.magFiltering.c_str(), atlas.mipmapMagFiltering.c_str());
		}
	}

	if (atlas.mipmapMin == false)
	{
		if (atlas.minFiltering == "nearest")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		}
		else if (atlas.minFiltering == "linear")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		else
		{
			this->log.error().format("Unknown minify filtering type: %s", atlas.minFiltering.c_str());
		}
	}
	else
	{
		if (atlas.minFiltering == "nearest" && atlas.mipmapMinFiltering == "nearest")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		}
		else if (atlas.minFiltering == "nearest" && atlas.mipmapMinFiltering == "linear")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
		}
		else if (atlas.minFiltering == "linear" && atlas.mipmapMinFiltering == "nearest")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		}
		else if (atlas.minFiltering == "linear" && atlas.mipmapMinFiltering == "linear")
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		}
		else
		{
			this->log.error().format("Unknown minify filtering type: %s + %s mipmap", atlas.minFiltering.c_str(), atlas.mipmapMinFiltering.c_str());
		}
	}

	glTexImage2D(GL_TEXTURE_2D,
			0,
			colorMode,
			static_cast<GLsizei>(imageWidth),
			static_cast<GLsizei>(imageHeight),
			0,
			static_cast<GLenum>(colorMode),
			GL_UNSIGNED_BYTE,
			reinterpret_cast<const GLvoid*>(imagePixels.data()));

	if (atlas.mipmapMag == true || atlas.mipmapMin == true)
	{
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	return buffer;
}

const TextureQuad& TextureManager::getTexture(const std::string& textureName) const
{
	auto textureIt = this->textureMap.find(textureName);

	if (textureIt != this->textureMap.end())
	{
		return textureIt->second;
	}
	else
	{
		this->log.error().format("Did not find texture with name \"%s\", returning default texture.", textureName.c_str());
		return this->defaultQuad;
	}
}

bool TextureManager::hasTexture(const std::string& textureName) const
{
	const auto& textureIt = this->textureMap.find(textureName);
	if (textureIt != this->textureMap.end())
	{
		return true;
	}

	return false;
}

unsigned int TextureManager::getNumAtlases() const
{
	return (unsigned int)this->textureAtlasFiles.size();
}

GLuint TextureManager::getTextureBuffer(unsigned int atlasNumber) const
{
	assert(atlasNumber < this->textureAtlasFiles.size());

	return this->textureAtlasFiles[atlasNumber].boundTextureBuffer;
}

GLuint TextureManager::getTextureBuffer(const std::string& atlasName) const
{
	for (const TextureAtlas& atlas : this->textureAtlasFiles)
	{
		if (atlas.name == atlasName)
		{
			return atlas.boundTextureBuffer;
		}
	}

	this->log.error().format("getTextureBuffer: No bound texture for atlas %s", atlasName.c_str());
	return 0;
}

glm::ivec2 TextureManager::getAtlasSize(const std::string& atlasName) const
{
	for (const TextureAtlas& atlas : this->textureAtlasFiles)
	{
		if (atlas.name == atlasName)
		{
			return glm::ivec2(atlas.width, atlas.height);
		}
	}

	this->log.error().format("getAtlasSize: No atlas with name %s", atlasName.c_str());
	return glm::ivec2(0, 0);
}

}
} }
