/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlBuffer.h>

namespace nox { namespace app { namespace graphics {

GlBuffer::GlBuffer(const GLenum type, const GLenum usage, const GLuint name):
	type(type),
	usage(usage),
	name(name),
	size(0)
{
}

GlBuffer::GlBuffer(const GLenum type, const GLuint name):
	GlBuffer(type, GL_STREAM_DRAW, name)
{
}

GlBuffer::GlBuffer():
	GlBuffer(GL_ARRAY_BUFFER, 0)
{
}

GlBuffer::GlBuffer(GlBuffer&& other):
	type(other.type),
	usage(other.usage),
	name(other.name),
	size(other.size)
{
	other.name = 0;
}

GlBuffer& GlBuffer::operator=(GlBuffer&& other)
{
	this->type = other.type;
	this->usage = other.usage;
	this->name = other.name;
	this->size = other.size;
	other.name = 0;

	return *this;
}

GlBuffer::~GlBuffer()
{
	if (this->name != 0)
	{
		glDeleteBuffers(1, &this->name);
	}
}

void GlBuffer::verifyValidity()
{
	if (this->name <= 0)
	{
		throw std::logic_error("Tried modifying invalid buffer");
	}
}

void GlBuffer::ensureSufficientSpace(const std::size_t size, GlContextState* state, GlBufferUploader& uploader)
{
	if (size > this->size)
	{
		this->size = math::upperPowerOfTwo<decltype(this->size)>(size);
		uploader.resizeBuffer(*this, this->size, state);
	}
}

void GlBuffer::bind(GlContextState* state)
{
	assert(state != nullptr);
	state->bindBuffer(this->type, this->name);
}

bool GlBuffer::isValid() const
{
	return this->name != 0;
}

GLenum GlBuffer::getType() const
{
	return this->type;
}

GLenum GlBuffer::getUsage() const
{
	return this->usage;
}

GLuint GlBuffer::getName() const
{
	return this->name;
}

GlBuffer GlBuffer::generate(const GLenum type, const GLenum usage)
{
	auto name = GLuint(0);
	glGenBuffers(1, &name);

	return GlBuffer(type, usage, name);
}

GlBuffer GlBuffer::generateArrayBuffer(const GLenum usage)
{
	return generate(GL_ARRAY_BUFFER, usage);
}

GlBuffer GlBuffer::generateElementArrayBuffer(const GLenum usage)
{
	return generate(GL_ELEMENT_ARRAY_BUFFER, usage);
}

} } }
