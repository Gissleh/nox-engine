/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/GlContextState.h>
#include <nox/util/string_utils.h>

#include <format.h>
#include <vector>
#include <regex>
#include <cassert>
#include <cstring>

namespace nox { namespace app
{
namespace graphics
{

std::size_t getMaxGlColorAttachments()
{
#if !NOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS
	return 1u;
#else
	auto max = GLint(0);
	glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &max);
	assert(max >= 0);
	return static_cast<std::size_t>(max);
#endif
}

bool supportGlMultisampling()
{
#if NOX_OPENGL_SUPPORT_MULTISAMPLING
	return true;
#else
	return false;
#endif
}

bool supportGlVao()
{
#if NOX_OPENGL_SUPPORT_VAO
	return true;
#else
	return false;
#endif
}

bool supportGlMultiColorAttachments()
{
#if NOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS
	return true;
#else
	return false;
#endif
}

bool supportGlMapBuffer()
{
#if NOX_OPENGL_SUPPORT_MAPBUFFER
	return true;
#else
	return false;
#endif
}

bool supportGlRenderbufferRgba8()
{
#if NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR < 3
	return false;
#else
	return true;
#endif
}

bool supportGlFragDataLocation()
{
#if NOX_OPENGL_SUPPORT_FRAGDATALOCATION
	return true;
#else
	return false;
#endif
}

bool linkShaderProgram(const GLuint shaderProgram, const GLuint vertexShader, const GLuint fragmentShader)
{
    assert(shaderProgram != 0);
    assert(vertexShader != 0);
    assert(fragmentShader != 0);

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glLinkProgram(shaderProgram);

    if (checkLinkStatus(shaderProgram) == false)
    {
        return false;
    }

    if (vertexShader)
    {
        glDetachShader(shaderProgram, vertexShader);
        glDeleteShader(vertexShader);
    }
    if (fragmentShader)
    {
        glDetachShader(shaderProgram, fragmentShader);
        glDeleteShader(fragmentShader);
    }

    return true;
}

bool linkShaderProgram(const GLuint shaderProgram, const GlslShader& vertexShader, const GlslShader& fragmentShader)
{
	return linkShaderProgram(shaderProgram, vertexShader.getId(), fragmentShader.getId());
}

GlslShader createShader(const std::string& shaderContent, const GLenum shaderType, const std::vector<GlslShader::VariableLocation>& variableLocations)
{
	GlslShader shader;
	shader.create(shaderType);

	if (shader.isCreated() == true)
	{
		shader.compile(shaderContent);

		for (const auto& location : variableLocations)
		{
			shader.addVariableLocation(location);
		}
	}

	return shader;
}

GlslShader createShader(std::istream& shaderFile, const GLenum shaderType, const std::vector<GlslShader::VariableLocation>& variableLocations)
{
	std::string shaderString;
	shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

	return createShader(shaderString, shaderType, variableLocations);
}

unsigned int generateGlslVersion(const GlVersion glVersion)
{
	auto glslVersion = unsigned(0);

	if (glVersion.isDesktop())
	{
		if (glVersion.getMajor() >= 4 || (glVersion.getMajor() == 3 && glVersion.getMinor() >= 3))
		{
			glslVersion = glVersion.getMajor() * 100u + glVersion.getMinor() * 10u;
		}
		else
		{
			glslVersion = 100u;

			if (glVersion.getMajor() == 3)
			{
				glslVersion = glslVersion + ((3u + glVersion.getMinor()) * 10u);
			}
			else
			{
				glslVersion = glslVersion + ((1u + glVersion.getMinor()) * 10u);
			}
		}
	}
	else if (glVersion.isEs())
	{
		if (glVersion.getMajor() == 2)
		{
			glslVersion = 100u;
		}
		else if (glVersion.getMajor() >= 3)
		{
			glslVersion = glVersion.getMajor() * 100u + glVersion.getMinor() * 10u;
		}
		else
		{
			throw std::runtime_error(fmt::format("OpenGL ES {}.{} GLSL not supported.", glVersion.getMajor(), glVersion.getMinor()));
		}
	}
	else
	{
		throw std::runtime_error("GLSL not supported for current OpenGL type.");
	}

	return glslVersion;
}

std::string generateGlslVersionString(const GlVersion& glVersion, const unsigned int glslVersion)
{
	auto versionString = "#version " + util::toString(glslVersion);

	if (glVersion.isEs() && glslVersion >= 300)
	{
		versionString += " es";
	}

	versionString += '\n';

	return versionString;
}

std::string generateGlslVersionString(const std::string& requiredVersionString, const GlVersion glVersion)
{
	const auto requiredGlslVersion = util::stringToNumber<unsigned int>(requiredVersionString);
	assert(requiredGlslVersion > 110 && requiredGlslVersion < 500);

	const auto currentGlslVersion = generateGlslVersion(glVersion);

	if (currentGlslVersion < requiredGlslVersion)
	{
		throw std::runtime_error(fmt::format("Current GLSL version ({}) lower than required version ({}).", currentGlslVersion, requiredGlslVersion));
	}

	return generateGlslVersionString(glVersion, currentGlslVersion);
}

std::string convertShaderToVersion(const std::string& originalShader, const GLenum shaderType, const GlVersion glVersion, const unsigned int glslVersion, std::vector<GlslShader::VariableLocation>& variableLocations)
{
	std::string shader = originalShader;

	const auto versionLine = generateGlslVersionString(glVersion, glslVersion);
	shader.insert(0, versionLine);

	if (glVersion.isEs())
	{
		shader.insert(versionLine.size(), "precision mediump float;\n");
	}

	const auto usesVarying = (glVersion.isEs() && glVersion.getMajor() < 3) || (glVersion.isDesktop() && glVersion.getMajor() < 3);
	const auto usesAttribute = usesVarying;
	const auto supportsOutLocation = (glVersion.isEs() && glVersion.getMajor() >= 2) || (glVersion.isDesktop() && (glVersion.getMajor() >= 4 || (glVersion.getMajor() >= 3 && glVersion.getMinor() >= 3)));


	// E.g. "vec2 color[2]"
	// Captures variable name (color).
	const auto variableDeclRegexStr = std::string(
		"[_[:alnum:]]+[[:space:]]+" // Type
		"([_[:alnum:]]+)" // Name
		"(?:\\[[[:digit:]]+\\])?" // Array size
		"[[:space:]]*;" // End
		);

	// E.g. "layout("
	const auto layoutStartRegStr = std::string("layout[[:space:]]*\\(");

	// E.g. ")"
	const auto layoutEndRegStr = std::string("\\)[[:space:]]+");

	// E.g. "location = "
	const auto locationStartRegStr = std::string("[[:space:]]*location[[:space:]]*=[[:space:]]*");

	// E.g. "0"
	const auto locationNumRegStr = std::string("[[:digit:]]");

	// E.g. ""
	const auto locationEndRegStr = std::string("[[:space:]]*");

	// E.g. "location = 1"
	const auto locationRegStr = locationStartRegStr + locationNumRegStr + locationEndRegStr;

	// E.g. "layout(location = 1)"
	// Captures nothing.
	const auto layoutLocationRegStr = std::string(layoutStartRegStr + locationRegStr + layoutEndRegStr);

	// E.g. "layout(location = 1) in vec2 color[2]"
	// Captures variable declaration (vec2 color[2])
	const auto inRegex = std::regex("(?:" + layoutLocationRegStr + ")?in[[:space:]]+(" + variableDeclRegexStr + ")");

	// E.g. "layout(location = 1) out vec2 color[2]"
	// Captures variable declaration (vec2 color[2])
	const auto outRegex = std::regex("(?:" + layoutLocationRegStr + ")?out[[:space:]]+(" + variableDeclRegexStr + ")");

	// The end of a regex search.
	const auto regexEndIt = std::sregex_iterator();

	/*
	 * Replace 'in' and 'out' with proper legacy names, like 'varying' and 'attribute'.
	 * Raplce 'texture' with 'texture2D'.
	 * Only one fragment shader output is supported in these versions, and that output
	 * will be converted to gl_FragColor here.
	 */
	if (usesVarying == true && shaderType == GL_VERTEX_SHADER)
	{
		shader = std::regex_replace(shader, outRegex, "varying $1");
	}

	if (usesAttribute == true && shaderType == GL_VERTEX_SHADER)
	{
		shader = std::regex_replace(shader, inRegex, "attribute $1");
	}

	if (usesVarying == true && shaderType == GL_FRAGMENT_SHADER)
	{
		shader = std::regex_replace(shader, inRegex, "varying $1");
	}

	if ((usesVarying == true || supportsOutLocation == false) && shaderType == GL_FRAGMENT_SHADER)
	{
		const auto outBeginIt = std::sregex_iterator(shader.begin(), shader.end(), outRegex);
		const auto numOuts = std::distance(outBeginIt, regexEndIt);

		// Remove "layout(location = 1)" and use glFragOutLocation in stead.
		// When using varying, only one output is supported, so there are no use for specifying the location,
		// thus we drop this in that case.
		if (supportsOutLocation == false && usesVarying == false)
		{
			auto outIt = outBeginIt;

			while (outIt != regexEndIt)
			{
				const auto outMatch = *outIt;

				// The match plus variable declaration capture and variable name capture.
				assert(outMatch.size() >= 3);
				const auto variableName = outMatch.str(2);

				// Find and store the location of the output. Then remove the "layout(location = 1)"
				const auto declaration = outMatch.str();
				const auto layoutLocationCaptureRegStr = std::string(layoutStartRegStr + locationStartRegStr + "(" + locationNumRegStr + ")" + locationEndRegStr + layoutEndRegStr);
				const auto layoutLocationCaptureRegex = std::regex(layoutLocationCaptureRegStr);
				auto layoutLocationMatch = std::smatch();
				if (std::regex_search(declaration, layoutLocationMatch, layoutLocationCaptureRegex) == true)
				{
					auto location = GlslShader::VariableLocation();
					location.variableName = variableName;
					location.location = util::stringToNumber<std::size_t>(layoutLocationMatch.str(1));

					variableLocations.push_back(std::move(location));

					const auto declarationPos = static_cast<std::string::size_type>(outMatch.position() + layoutLocationMatch.position());
					const auto layoutLocationLength = static_cast<std::string::size_type>(layoutLocationMatch.length());

					// This looks a bit hacky, but improves performance as we don't have to resize the string when just replacing
					// with spaces. Also it keeps the positions of each mach consistent. Might this fail with unicode?
					shader.replace(
						declarationPos,
						layoutLocationLength,
						layoutLocationLength,
						' ');

				}

				++outIt;
			}
		}

		if (usesVarying == true)
		{
			if (numOuts == 1)
			{
				const auto outMatch = *outBeginIt;

				// The match plus variable declaration capture and variable name capture.
				assert(outMatch.size() >= 3);

				shader = std::regex_replace(shader, outRegex, "$1");

				const auto variableName = outMatch.str(2);

				const auto variableAssignRegex = std::regex(variableName + "([[:space:]]+(=|\\+=|-=)[[:space:]]+.*;)");

				const auto assignBeginIt = std::sregex_iterator(shader.begin(), shader.end(), variableAssignRegex);
				const auto numAssigns = std::distance(assignBeginIt, regexEndIt);
				const auto assignLastIt = std::next(assignBeginIt, numAssigns - 1);

				const auto lastAssignMatch = *assignLastIt;
				const auto fragAssignPos = static_cast<std::string::size_type>(lastAssignMatch.position() + lastAssignMatch.length() + 1);
				shader.insert(fragAssignPos, "gl_FragColor = " + variableName + ";\n");
			}
			else if (numOuts > 1)
			{
				throw std::runtime_error("This version of GLSL does not support multiple outputs from the fragment shader.");
			}
		}

		// TODO: Check if texture2D or textureCube should be used based on the parameters.
		const auto textureRegex = std::regex("texture([[:space:]]*\\([[:space:]]*([_[:alnum:]])+[[:space:]]*,.*\\).*;)");
		shader = std::regex_replace(shader, textureRegex, "texture2D$1");
	}

	return shader;
}

GlslShader createShaderAutoVersion(std::istream& shaderFile, const GLenum shaderType, const GlVersion glVersion)
{
	std::string shaderString;
	shaderString.assign(std::istreambuf_iterator<char>(shaderFile), std::istreambuf_iterator<char>());

	const auto glslVersion = generateGlslVersion(glVersion);
	std::vector<GlslShader::VariableLocation> variableLocations;
	shaderString = convertShaderToVersion(shaderString, shaderType, glVersion, glslVersion, variableLocations);

	return createShader(shaderString, shaderType, variableLocations);
}

GlslShader createShaderAutoVersion(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType, const GlVersion glVersion, std::string& error)
{
	const auto shaderHandle = resourceCache->getHandle(resource::Descriptor(shaderPath));

	if (!shaderHandle)
	{
		error = "Could not open shader resource \"" + shaderPath + "\"";
		return GlslShader();
	}

	std::istringstream shaderStream(std::string(shaderHandle->getResourceBuffer(), shaderHandle->getResourceBufferSize()));

	auto shader = createShaderAutoVersion(shaderStream, shaderType, glVersion);

	return shader;
}

GlslVertexAndFragmentShader createVertexAndFragmentShaderAutoVersion(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GlVersion glVersion, std::string& error)
{
	GlslVertexAndFragmentShader shader;

	std::string vertexError;
	shader.vertex = createShaderAutoVersion(resourceCache, shaderPath + ".vert", GL_VERTEX_SHADER, glVersion, vertexError);

	if (shader.vertex.isCreated() == false)
	{
		error = vertexError;
	}
	else if (shader.vertex.isCompiled() == false)
	{
		error = shader.vertex.getCompileLog();
	}
	else
	{
		std::string fragError;
		shader.fragment = createShaderAutoVersion(resourceCache, shaderPath + ".frag", GL_FRAGMENT_SHADER, glVersion, fragError);

		if (shader.fragment.isCreated() == false)
		{
			error = fragError;
		}
		else if (shader.fragment.isCompiled() == false)
		{
			error = shader.fragment.getCompileLog();
		}
	}

	return shader;
}

bool checkLinkStatus(const GLuint program)
{
    auto status = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &status);

    if (status == GL_FALSE) {
        return false;
    }

    return true;
}

bool checkCompileStatus(const GLuint shaderId)
{
    GLint status;
    glGetShaderiv(shaderId, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE)
    {
        return false;
    }

    return true;
}

GLuint createTexture(
	GlContextState& state,
	const GLsizei textureWidth,
	const GLsizei textureHeight,
	const GLint internalFormat,
	const GLenum format,
	const GLint filtering
)
{
	GLuint texture = 0;

	glGenTextures(1, &texture);
	state.bindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, textureWidth, textureHeight, 0, format, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering);

	/*
	 * This was GL_CLAMP_TO_BORDER pre-opengles. OpenGL ES2 doesn't support
	 * GL_CLAMP_TO_BORDER. This might cause undesired results, so another
	 * solution may have to be implemented.
	 */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	return texture;
}

FrameBuffer createFrameBufferObject(const GLsizei FBOWidth, const GLsizei FBOHeight, const std::vector<GLuint>& textures, const bool hasDepthAndStencilBuffer)
{
	GLuint FBO = 0;
	GLuint colorRenderBuffer = 0;
	GLuint stencilRenderbuffer = 0;

	glGenFramebuffers(1,&FBO);
	glBindFramebuffer(GL_FRAMEBUFFER,FBO);

	if (textures.size() == 1)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[0], 0);
	}
	else if (textures.size() > 1)
	{
		if (textures.size() > getMaxGlColorAttachments())
		{
			throw std::runtime_error(fmt::format("Tried creating framebuffer object with more color attachments than allowed. Wanted: {}. Max: {}.", textures.size(), getMaxGlColorAttachments()));
		}

		std::vector<GLenum> colorAttachments;
		colorAttachments.reserve(textures.size());

		for (unsigned int i = 0; i < textures.size(); i++)
		{
			const auto attachment = GL_COLOR_ATTACHMENT0 + i;
			colorAttachments.push_back(attachment);
			glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, textures[i], 0);
		}

		glDrawBuffers(static_cast<GLsizei>(textures.size()), colorAttachments.data());
	}
	else
	{
		glGenRenderbuffers(1, &colorRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);

		/*
		 * This was GL_RGBA8 pre-opengles. OpenGL ES2 doesn't support
		 * GL_RGBA8. This might cause undesired results (low-quality textures), so another
		 * solution may have to be implemented.
		 */
		auto format = GL_RGBA4;
		if (supportGlRenderbufferRgba8())
		{
			format = GL_RGBA8;
		}
		glRenderbufferStorage(GL_RENDERBUFFER, static_cast<GLenum>(format), FBOWidth, FBOHeight);

		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
	}

	if (hasDepthAndStencilBuffer)
	{
		glGenRenderbuffers(1, &stencilRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, stencilRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, stencilRenderbuffer);
	}

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assert(status == GL_FRAMEBUFFER_COMPLETE);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
//		g_systemLogger->error("Framebuffer object created was not complete.");
	}

	glBindFramebuffer(GL_FRAMEBUFFER,0);

	return FrameBuffer(FBO, { colorRenderBuffer, stencilRenderbuffer });
}

#if NOX_OPENGL_SUPPORT_MULTISAMPLE
FrameBuffer createMultisampleFrameBufferObject(const GLsizei FBOWidth, const GLsizei FBOHeight, const std::vector<GLuint>& textures, const GLsizei samples, const bool hasDepthAndStencilBuffer)
{
	GLuint FBO = 0;
	GLuint colorRenderBuffer = 0;
	GLuint stencilRenderbuffer = 0;

	glGenFramebuffers(1,&FBO);
	glBindFramebuffer(GL_FRAMEBUFFER,FBO);

	if (textures.size() == 1)
	{
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textures[0], 0);
	}
	else if (textures.size() > 1)
	{
		if (textures.size() > getMaxGlColorAttachments())
		{
			throw std::runtime_error(fmt::format("Tried creating framebuffer object with more color attachments than allowed. Wanted: %u. Max: %u.", textures.size(), getMaxGlColorAttachments()));
		}

		std::vector<GLenum> colorAttachments;
		colorAttachments.reserve(textures.size());

		for (unsigned int i = 0; i < textures.size(); i++)
		{
			const auto attachment = GL_COLOR_ATTACHMENT0 + i;
			colorAttachments.push_back(attachment);
			glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, textures[i], 0);
		}

		glDrawBuffers(static_cast<GLsizei>(textures.size()), colorAttachments.data());
	}
	else
	{
		glGenRenderbuffers(1, &colorRenderBuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
		glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, GL_RGBA8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, colorRenderBuffer);
	}

	if (hasDepthAndStencilBuffer)
	{
		glGenRenderbuffers(1, &stencilRenderbuffer);
		glBindRenderbuffer(GL_RENDERBUFFER, stencilRenderbuffer);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, FBOWidth, FBOHeight);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER,  GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, stencilRenderbuffer);
	}

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	assert(status == GL_FRAMEBUFFER_COMPLETE);

	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
//		g_systemLogger->error("Multisampled framebuffer object created was not complete.");
	}

	glBindFramebuffer(GL_FRAMEBUFFER,0);

	return FrameBuffer(FBO, { colorRenderBuffer, stencilRenderbuffer });
}
#endif

std::string getShaderCompileLog(const GLuint shaderId)
{
        GLint length;
        glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &length);

        std::vector<GLchar> log(static_cast<std::vector<GLchar>::size_type>(length), ' ');
        glGetShaderInfoLog(shaderId, length, &length, log.data());

        return std::string(log.data());
}

void deleteFramebufferIfValid(GLuint& name)
{
	if (name != 0)
	{
		glDeleteFramebuffers(1, &name);
		name = 0;
	}
}

void deleteTextureIfValid(GLuint& name)
{
	if (name != 0)
	{
		glDeleteTextures(1, &name);
		name = 0;
	}
}

void GlslShader::create(const GLenum type)
{
	this->id = glCreateShader(type);
}

bool GlslShader::compile(const std::string& source)
{
	return this->compile(source.c_str());
}

bool GlslShader::compile(const char* source)
{
	glShaderSource(this->id, 1, &source, nullptr);

	glCompileShader(this->id);

	if (checkCompileStatus(this->id) == false)
	{
		this->compiled = false;
		return false;
	}
	else
	{
		this->compiled = true;
		return true;
	}
}

void GlslShader::addVariableLocation(const VariableLocation& location)
{
	this->variableLocationList.push_back(location);
}

void GlslShader::bindFragDataLocations(const GlslProgram& program) const
{
#if NOX_OPENGL_SUPPORT_FRAGDATALOCATION
	for (const auto& location : this->variableLocationList)
	{
		glBindFragDataLocation(program.getId(), static_cast<GLuint>(location.location), location.variableName.c_str());
	}
#else
	throw std::logic_error("The current OpenGL version does not support glFragDataLocation.");
#endif
}

void GlslShader::destroy()
{
	if (this->id != 0)
	{
		glDeleteShader(this->id);
		this->id = 0;
		this->compiled = false;
	}
}

bool GlslShader::isCreated() const
{
	return this->id != 0;
}

bool GlslShader::isCompiled() const
{
	return this->compiled;
}

GLuint GlslShader::getId() const
{
	return this->id;
}

std::string GlslShader::getCompileLog() const
{
	return getShaderCompileLog(this->id);
}

void GlslProgram::create()
{
	this->id = glCreateProgram();
}

bool GlslProgram::link(const GlslShader& vert, const GlslShader& frag)
{
	if (supportGlFragDataLocation())
	{
		frag.bindFragDataLocations(*this);
	}

	return this->link(vert.getId(), frag.getId());
}

bool GlslProgram::link(const GLuint vert, const GLuint frag)
{
	assert(vert != 0);
	assert(frag != 0);
	assert(this->id != 0);

	glAttachShader(this->id, vert);
	glAttachShader(this->id, frag);

	glLinkProgram(this->id);

	if (checkLinkStatus(this->id) == false)
	{
		return false;
	}

	glDetachShader(this->id, vert);
	glDeleteShader(vert);
	glDetachShader(this->id, frag);
	glDeleteShader(frag);

	if (checkLinkStatus(this->id) == false)
	{
		this->linked = false;
		return false;
	}
	else
	{
		this->linked= true;
		return true;
	}
}

void GlslProgram::destroy()
{
	if (this->id != 0)
	{
		glDeleteProgram(this->id);
		this->id = 0;
		this->linked = false;
	}
}

bool GlslProgram::isCreated() const
{
	return this->id != 0;
}

bool GlslProgram::isLinked() const
{
	return this->linked;
}

GLuint GlslProgram::getId() const
{
	return this->id;
}

std::string GlslProgram::getLinkLog() const
{
        GLint length = 0;
        glGetProgramiv(this->id, GL_INFO_LOG_LENGTH, &length);

        std::vector<char> log((unsigned int)length);
        glGetProgramInfoLog(this->id, length, &length, &log[0]);

        return std::string(&log.front());
}

FrameBuffer::FrameBuffer() = default;

FrameBuffer::FrameBuffer(const GLuint name, const std::vector<GLuint>& renderBuffers):
	bufferName(name),
	renderBuffernNames(renderBuffers)
{
}

FrameBuffer::FrameBuffer(FrameBuffer&& other):
	bufferName(other.bufferName),
	renderBuffernNames(std::move(other.renderBuffernNames))
{
	other.bufferName = 0;
}

FrameBuffer& FrameBuffer::operator=(FrameBuffer&& other)
{
	this->bufferName = other.bufferName;
	this->renderBuffernNames = std::move(other.renderBuffernNames);

	other.bufferName = 0;

	return *this;
}

FrameBuffer::~FrameBuffer()
{
	if (this->bufferName != 0)
	{
		glDeleteRenderbuffers(static_cast<GLsizei>(this->renderBuffernNames.size()), this->renderBuffernNames.data());

		glDeleteRenderbuffers(1, &this->bufferName);
	}
}

GLuint FrameBuffer::getId() const
{
	return this->bufferName;
}

void FrameBuffer::bind(const GLenum target)
{
	glBindFramebuffer(target, this->bufferName);
}

FrameBuffer FrameBuffer::getScreenBuffer()
{
	return FrameBuffer(0, {});
}

bool GlslVertexAndFragmentShader::isValid() const
{
	return (this->vertex.isCompiled() && this->fragment.isCompiled());
}

GlVersion::GlVersion(const std::uint8_t major, const std::uint8_t minor, const Type type):
	glMajor(major),
	glMinor(minor),
	type(type)
{
}

std::uint8_t GlVersion::getMajor() const
{
	return this->glMajor;
}

std::uint8_t GlVersion::getMinor() const
{
	return this->glMinor;
}

bool GlVersion::isDesktop() const
{
	return this->type == Type::DESKTOP;
}

bool GlVersion::isEs() const
{
	return this->type == Type::ES;
}

}
}
}
