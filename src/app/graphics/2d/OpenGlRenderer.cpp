/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/IContext.h>
#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/SubRenderer.h>
#include <nox/app/resource/Handle.h>
#include <json/reader.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <nox/app/graphics/2d/Camera.h>
#include <nox/app/graphics/2d/Geometry.h>
#include <nox/app/graphics/2d/Light.h>
#include <nox/app/graphics/2d/BackgroundGradient.h>
#include <nox/app/graphics/2d/DebugRenderer.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>
#include <nox/app/graphics/2d/StenciledTiledTextureRenderer.h>
#include <nox/app/graphics/2d/TiledTextureRenderer.h>
#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/app/graphics/2d/StenciledTiledTextureGenerator.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/util/algorithm.h>
#include <SDL.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>

namespace nox { namespace app { namespace graphics
{

namespace
{

std::vector<TextureQuad::VertexAttribute> makeTriangleStripBox(const math::Box<glm::vec2>& box);

}

OpenGlRenderer::OpenGlRenderer():
	NUM_RENDER_LEVELS(10000),
	context(nullptr),
	initialized(false),

	lightStartRenderLevel(0),
	renderCamera(std::make_shared<Camera>(glm::uvec2(1, 1))),
	debugRenderingEnabled(false),
	windowSizeChanged(false),

	lightLevel(1.0f),

	lightClearColor(glm::vec4(0)),
	renderClearColor(glm::vec4(0)),

	renderTexture(0),
	lightLuminanceTexture(0),

	superSampleMultiplier(1.0f),
	lightMapMultisamplingSamples(0),
	lightTextureSizeMultiplier(0.3f),
	effectTextureSizeMultiplier(0.3f),

	litGameplayPresentTexture(0),
	litGameplayPresentRenderUniform(-1),
	litGameplayPresentLightUniform(-1),
	litGameplayPresentLightLuminanceUniform(-1),
	litGameplayPresentDayLightCoefficiencyUniform(-1),

	effectGameplayPresentRenderUniform(-1),
	effectGameplayPresentLightUniform(-1),

	effectMapAttributes(nullptr),
	effectMapTexture(0),

	lightMapAttributes(nullptr),
	lightMapTexture(0),

	lightHorizontalBlurTexture(0),

	lightHorizontalBlurTextureUniform(-1),
	lightHorizontalBlurSizeUniform(-1),

	lightVerticalBlurTexture(0),

	lightVerticalBlurTextureUniform(0),
	lightVerticalBlurSizeUniform(0),

	lightTexture(0),

	blurEnabled(true),
	pixelsPerBlurStep(1),
	numberOfStaticLightCoords(0),
	staticLightsUpdated(false),
	dynamicLightsUpdated(false),
	lightCoordsUpdated(false),
	effectLightsUpdated(false),
	effectCoordsUpdated(false),
	textureRenderer(NUM_RENDER_LEVELS),
	worldAtlasTextureBuffer(0),

	debugDataPreparationStartBarrier(2),
	debugDataPreparationFinishedBarrier(2),
	quitDebugDataPreparation(false),

	textureDataPreparationStartBarrier(2),
	textureDataPreparationFinishedBarrier(2),
	quitTextureDataPreparation(false),

	lightingDataPreparationStartBarrier(2),
	lightingDataPreparationFinishedBarrier(2),
	quitLightingDataPreparation(false),

	effectDataPreparationStartBarrier(2),
	effectDataPreparationFinishedBarrier(2),
	quitEffectDataPreparation(false),

	subRenderersDataPreparationStartBarrier(2),
	subRenderersDataPreparationFinishedBarrier(2),
	quitSubRenderersDataPreparation(false)
{
	this->debugRenderer = std::unique_ptr<DebugRenderer>(new DebugRenderer());
}

OpenGlRenderer::~OpenGlRenderer()
{
	this->quitDebugDataPreparation = true;
	this->debugDataPreparationStartBarrier.wait();
	this->debugDataPreparationFinishedBarrier.wait();

	if (debugDataPreparerThread.joinable())
	{
		debugDataPreparerThread.join();
	}

	this->quitTextureDataPreparation = true;
	this->textureDataPreparationStartBarrier.wait();
	this->textureDataPreparationFinishedBarrier.wait();

	if (textureDataPreparerThread.joinable())
	{
		textureDataPreparerThread.join();
	}

	this->quitLightingDataPreparation = true;
	this->lightingDataPreparationStartBarrier.wait();
	this->lightingDataPreparationFinishedBarrier.wait();

	if (lightingDataPreparerThread.joinable())
	{
		lightingDataPreparerThread.join();
	}

	this->quitEffectDataPreparation = true;
	this->effectDataPreparationStartBarrier.wait();
	this->effectDataPreparationFinishedBarrier.wait();

	if (effectDataPreparerThread.joinable())
	{
		effectDataPreparerThread.join();
	}

	this->quitSubRenderersDataPreparation = true;
	this->subRenderersDataPreparationStartBarrier.wait();
	this->subRenderersDataPreparationFinishedBarrier.wait();

	if (subRenderersDataPreparerThread.joinable())
	{
		subRenderersDataPreparerThread.join();
	}
}

bool OpenGlRenderer::init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize)
{
	assert(context != nullptr);

	this->context = context;

	this->log = context->createLogger();
	this->log.setName("OpenGLRenderer");

	this->shaderDirectory = shaderDirectory;

	this->textureManager.setLogger(context->createLogger());
	this->blockingBufferUploader = GlBlockingBufferUploader(context->createLogger());
	this->asyncBufferUploader = GlMaybeAsyncBufferUploader(context->createLogger());

	resource::IResourceAccess* resourceCache = context->getResourceAccess();
	assert(resourceCache != nullptr);

	if (this->initOpenGL(windowSize) == false)
	{
		return false;
	}

	if (this->setupSpriteRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupDebugRendering(resourceCache, shaderDirectory) == false)
	{
		return false;
	}

	if (this->setupWindowTextureRendering() == false)
	{
		return false;
	}

	for (auto& subRenderer : this->subRenderers)
	{
		subRenderer->init(this->context, this->renderData, windowSize);
	}

	if (this->blurEnabled == true)
	{
		this->lightTexture = this->lightVerticalBlurTexture;
	}
	else
	{
		this->lightTexture = this->lightMapTexture;
	}

	this->debugDataPreparerThread = std::thread(&OpenGlRenderer::debugDataPreparation, this);
	this->textureDataPreparerThread = std::thread(&OpenGlRenderer::textureDataPreparation, this);
	this->lightingDataPreparerThread = std::thread(&OpenGlRenderer::lightingDataPreparation, this);
	this->effectDataPreparerThread = std::thread(&OpenGlRenderer::effectDataPreparation, this);
	this->subRenderersDataPreparerThread = std::thread(&OpenGlRenderer::subRenderersDataPreparation, this);

	this->resizeWindow(this->windowSize);

	this->initialized = true;

	return true;
}

void OpenGlRenderer::loadTextureAtlases(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess)
{
	this->textureManager.readGraphicsFile(graphicsResourceDescriptor, resourceAccess);
	this->textureManager.writeToTextureBuffers(this->glState, resourceAccess);
}

void OpenGlRenderer::setWorldTextureAtlas(const std::string& atlasName)
{
	this->worldAtlasTextureBuffer = this->textureManager.getTextureBuffer(atlasName);
	assert(this->worldAtlasTextureBuffer != 0);
}

void OpenGlRenderer::onRender()
{
	if (this->debugRenderingEnabled == true)
	{
		this->debugDataPreparationStartBarrier.wait();
	}

	// Start asynchronus data preparation.
	this->textureDataPreparationStartBarrier.wait();
	this->lightingDataPreparationStartBarrier.wait();
	this->effectDataPreparationStartBarrier.wait();
	this->subRenderersDataPreparationStartBarrier.wait();

	glFinish();

	this->effectDataPreparationFinishedBarrier.wait();

	if (this->effectCoords.empty() == false)
	{
		const auto& shaderProgram = this->shaders["effectMap"].shaderProgram;
		if (shaderProgram.isLinked() == false)
		{
			this->setupEffectMapRendering(this->context->getResourceAccess(), this->shaderDirectory);
		}

		this->effectRenderingIo();
	}

	this->lightingDataPreparationFinishedBarrier.wait();

	if (this->lightCoords.empty() == false)
	{
		const auto& shaderProgram = this->shaders["lightMap"].shaderProgram;
		if (shaderProgram.isLinked() == false)
		{
			this->setupLightMapRendering(this->context->getResourceAccess(), this->shaderDirectory);
		}

		this->lightRenderingIo();
	}

	const glm::vec2 cameraPosition = this->renderCamera->getPosition();
	const glm::vec2 cameraHalfSize = this->renderCamera->getSize() / 2.0f;
	this->textureDataPreparationFinishedBarrier.wait();
	this->textureRenderer.setCullArea(math::Box<glm::vec2>(cameraPosition - cameraHalfSize, cameraPosition + cameraHalfSize), this->renderCamera->getRotation());
	this->textureRenderer.handleIo(this->renderData);

	if (this->backgroundGradient != nullptr)
	{
		if (this->backgroundGradient->needsBufferUpdate() == true)
		{
			this->backgroundGradient->handleIo(this->renderData);
		}

		if (this->lightCoords.empty() == true)
		{
			this->backgroundGradient->setColorMultiplier(this->lightLevel);
		}
		else
		{
			this->backgroundGradient->setColorMultiplier(1.0f);
		}
	}

	if (this->debugRenderingEnabled == true)
	{
		this->debugDataPreparationFinishedBarrier.wait();
		this->debugRenderer->onDebugIo(this->renderData, this->renderCamera->getViewProjectionMatrix());
	}

	if (this->lightCoords.empty() == true && this->effectCoords.empty() == true)
	{
		this->renderWorld(this->screenBuffer);
	}
	else
	{
		//  Render the world to a texture.
		this->renderWorld(this->gameplayFBO);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE);

		if (this->lightCoords.empty() == false)
		{
			//  Render the light map to a texture;
			this->onLightMapRender();
		}

		if (this->effectCoords.empty() == false)
		{
			//  Render the effect map to a texture;
			this->onEffectMapRender();
		}

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		if (this->effectCoords.empty() == true)
		{
			this->renderLitWorld(this->screenBuffer);
		}
		else
		{
			//  Render the world lit by the light map
			this->renderLitWorld(this->litGameplayPresentFBO);

			//  Render the lit world with effects.
			this->renderEffectWorldToScreen();
		}
	}

	//  Render the debug objects.
	if (this->debugRenderingEnabled == true)
	{
		std::lock_guard<std::mutex> debugLock(this->debugRenderMutex);

		this->debugRenderer->onDebugRender(this->renderData);
	}

	this->subRenderersDataPreparationFinishedBarrier.wait();
	for (auto& subRenderer : this->subRenderers)
	{
		subRenderer->render(this->renderData);
	}

	if (this->windowSizeChanged == true)
	{
	        this->windowSizeChanged = false;
	}

#ifndef NDEBUG
	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("OpenGL: %i", error);
	}
#endif
}



void OpenGlRenderer::setRootSceneNode(const std::shared_ptr<SceneGraphNode>& rootNode)
{
    this->sceneRoot = rootNode;
    this->sceneRoot->setCurrentRenderer(this);
}

void OpenGlRenderer::setCamera(const std::shared_ptr<Camera>& camera)
{
    this->renderCamera = camera;
}

bool OpenGlRenderer::toggleDebugRendering()
{
    this->debugRenderingEnabled = !this->debugRenderingEnabled;

    return this->debugRenderingEnabled;
}

void OpenGlRenderer::parseSceneGraph()
{
	if (this->sceneRoot != nullptr)
	{
		glm::mat4 transformationMatrix;
		this->sceneRoot->onTraverse(this->textureRenderer, transformationMatrix);
	}
}

void OpenGlRenderer::renderObjects()
{
	this->textureRenderer.drawData(this->renderData);
}

void OpenGlRenderer::renderWorld(FrameBuffer& frameBuffer)
{
	frameBuffer.bind(GL_FRAMEBUFFER);

	if (this->renderClearColor != this->currentClearColor)
	{
		glClearColor(renderClearColor.r, renderClearColor.g, renderClearColor.b, renderClearColor.a);
		this->currentClearColor = this->renderClearColor;
	}

	this->renderData.setStencilMask(0xFF);
	glClear(GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// We need to set the viewport to fit the new resolution (if we use supersampling).
	if (this->superSampleMultiplier != 1.0f)
	{
		glm::vec2 supersampledSize;
		supersampledSize.x = (float)this->windowSize.x * this->superSampleMultiplier;
		supersampledSize.y = (float)this->windowSize.y * this->superSampleMultiplier;

		glViewport(0, 0, (GLsizei)supersampledSize.x, (GLsizei)supersampledSize.y);
	}

	const glm::mat4& viewProjectionMatrix = this->renderCamera->getViewProjectionMatrix();

	assert(this->worldAtlasTextureBuffer > 0);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->worldAtlasTextureBuffer);

	if (supportGlMultiColorAttachments())
	{
		this->renderData.bindShaderProgram(this->shaders["spriteLightLuminance"].shaderProgram);
		glUniformMatrix4fv(this->shaders["spriteLightLuminance"].ViewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));

		if (this->lightCoords.empty() == true)
		{
			glUniform1f(this->spriteLightLuminanceColorMultiplierUniform, this->lightLevel);
		}
		else
		{
			glUniform1f(this->spriteLightLuminanceColorMultiplierUniform, 1.0f);
		}
	}

	this->renderData.bindShaderProgram(this->shaders["sprite"].shaderProgram);
	glUniformMatrix4fv(this->shaders["sprite"].ViewProjectionMatrixUniform, 1, GL_FALSE, glm::value_ptr(viewProjectionMatrix));

	if (this->lightCoords.empty() == true)
	{
		glUniform1f(this->spriteColorMultiplierUniform, this->lightLevel);
	}
	else
	{
		glUniform1f(this->spriteColorMultiplierUniform, 1.0f);
	}

	for (std::unique_ptr<RenderStep>& renderStep : this->renderSteps)
	{
		renderStep->render(this->renderData, viewProjectionMatrix);
	}

	if (this->superSampleMultiplier != 1.0f)
	{
		glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
	}
}

void OpenGlRenderer::renderLitWorld(FrameBuffer& framebuffer)
{
	framebuffer.bind(GL_FRAMEBUFFER);

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(this->lightClearColor.r, this->lightClearColor.g, this->lightClearColor.b, this->lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	if (this->litGameplayPresentProgram.isLinked() == false)
	{
		this->setupLitWorldRendering(this->context->getResourceAccess(), this->shaderDirectory);
	}

	this->renderData.bindShaderProgram(this->litGameplayPresentProgram);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->renderTexture);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightTexture);
	glUniform1i(this->litGameplayPresentLightUniform, 1);

	if (supportGlMultiColorAttachments())
	{
		glActiveTexture(GL_TEXTURE2);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->lightLuminanceTexture);
		glUniform1i(this->litGameplayPresentLightLuminanceUniform, 2);
	}

	glActiveTexture(GL_TEXTURE0);

	glUniform1f(this->litGameplayPresentDayLightCoefficiencyUniform, this->lightLevel);

	renderData.disable(GL_STENCIL_TEST);

	this->windowTextureModel.draw(GL_TRIANGLE_STRIP);
}

void OpenGlRenderer::renderEffectWorldToScreen()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(lightClearColor.r, lightClearColor.g, lightClearColor.b, lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	if (this->effectGameplayPresentProgram.isLinked() == false)
	{
		this->setupEffectMapRendering(this->context->getResourceAccess(), this->shaderDirectory);
	}

	this->renderData.bindShaderProgram(this->effectGameplayPresentProgram);

	auto worldTexture = this->litGameplayPresentTexture;
	if (this->lightCoords.empty() == true)
	{
		worldTexture = this->renderTexture;
	}

	this->renderData.bindTexture(GL_TEXTURE_2D, worldTexture);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->effectMapTexture);
	glUniform1i(this->effectGameplayPresentLightUniform, 1);
	glActiveTexture(GL_TEXTURE0);

	renderData.disable(GL_STENCIL_TEST);

	this->windowTextureModel.draw(GL_TRIANGLE_STRIP);
}

void OpenGlRenderer::renderLightHorizontalBlur()
{
	this->lightHorizontalBlurFBO.bind(GL_FRAMEBUFFER);
	glClear(GL_COLOR_BUFFER_BIT);

	if (this->lightHorizontalBlurProgram.isLinked() == false)
	{
		this->setupLightHorizontalBlur(this->context->getResourceAccess(), this->shaderDirectory);
	}

	this->renderData.bindShaderProgram(this->lightHorizontalBlurProgram);

	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightMapTexture);

	const float scaledWindowWidth = static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier;
	const float scaledWindowHeight = static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier;

	if (this->lightTextureSizeMultiplier != 1.0f)
	{
		// We need to set the viewport to fit the new resolution.
		glViewport(0, 0, static_cast<GLint>(scaledWindowWidth), static_cast<GLint>(scaledWindowHeight));
	}

	renderData.disable(GL_STENCIL_TEST);

	this->windowTextureModel.draw(GL_TRIANGLE_STRIP);

	glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
}


void OpenGlRenderer::renderLightVerticalBlur()
{
	this->lightVerticalBlurFBO.bind(GL_FRAMEBUFFER);
	glClear(GL_COLOR_BUFFER_BIT);

	if (this->lightVerticalBlurProgram.isLinked() == false)
	{
		this->setupLightVerticalBlur(this->context->getResourceAccess(), this->shaderDirectory);
	}

	this->renderData.bindShaderProgram(lightVerticalBlurProgram);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightHorizontalBlurTexture);

	const float scaledWindowWidth = static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier;
	const float scaledWindowHeight = static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier;

	if (this->lightTextureSizeMultiplier != 1.0f)
	{
		// We need to set the viewport to fit the new resolution.
		glViewport(0, 0, static_cast<GLint>(scaledWindowWidth), static_cast<GLint>(scaledWindowHeight));
	}

	renderData.disable(GL_STENCIL_TEST);

	this->windowTextureModel.draw(GL_TRIANGLE_STRIP);

	glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));

}

bool OpenGlRenderer::initOpenGL(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	this->currentClearColor = glm::vec4(1);
	this->lightClearColor = glm::vec4(0.0f, 0.0f, 0.0f, 1.0);
	this->renderClearColor = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

#if NOX_OPENGL_GLEW
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();

	// GLEW might trigger an error flag that we can safely ignore, so we flush it.
	glGetError();

	if (glewError != GLEW_OK)
	{
		glewGetErrorString(glewError);
		this->log.error().format("Error initializing GLEW: %s", (const char *) glewGetErrorString(glewError));
		return false;
	}
#endif

	auto glVersionCString = (const char *) glGetString(GL_VERSION);
	auto glVendorCString = (const char *) glGetString(GL_VENDOR);
	auto glslVersionCString = (const char *) glGetString(GL_SHADING_LANGUAGE_VERSION);

	// The reason we're copying the c-strings into std::strings is that the c-strings might be null.
	// At least this is the case on Android.
	std::string glVersionString;
	std::string glVendorString;
	std::string glslVersionString;

	if (glVersionCString)
	{
		glVersionString = glVersionCString;
	}
	if (glVendorCString)
	{
		glVendorString = glVendorCString;
	}
	if (glslVersionCString)
	{
		glslVersionString = glslVersionCString;
	}

	GLint glMajor = 0;
	GLint glMinor = 0;
#if NOX_OPENGL_DESKTOP || (NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR >= 3)
	glGetIntegerv(GL_MAJOR_VERSION, &glMajor);
	glGetIntegerv(GL_MINOR_VERSION, &glMinor);
#elif NOX_OPENGL_ES && NOX_OPENGL_VERSION_MAJOR == 2
	glMajor = 2;
	glMinor = 0;
#endif

	auto versionType = GlVersion::Type::DESKTOP;
#if NOX_OPENGL_ES
	versionType = GlVersion::Type::ES;
#endif

	this->glState = GlContextState(GlVersion(static_cast<std::uint8_t>(glMajor), static_cast<std::uint8_t>(glMinor), versionType));
	this->glState.setLogger(this->context->createLogger());
	this->renderData = RenderData(&this->glState, &this->textureManager);

	if ((versionType == GlVersion::Type::DESKTOP && glMajor < 3) || (versionType == GlVersion::Type::ES && glMajor < 2))
	{
		this->log.error().format("Unsupported hardware detected: OpenGL %u.%u, with GLSL \"%s\", using \"%s\" by \"%s\". OpenGL 3.0 or OpenGL ES 2.0 or better is required.",
			this->renderData.getGlVersion().getMajor(),
			this->renderData.getGlVersion().getMinor(),
			glslVersionString.c_str(),
			glVersionString.c_str(),
			glVendorString.c_str());

		return false;
	}

	this->log.verbose().format(
			"Initialized OpenGL %u.%u, with GLSL %s, using \"%s\" by \"%s\".",
			this->renderData.getGlVersion().getMajor(),
			this->renderData.getGlVersion().getMinor(),
			glslVersionString.c_str(),
			glVersionString.c_str(),
			glVendorString.c_str());

	this->renderData.disable(GL_DEPTH_TEST);
	this->renderData.disable(GL_STENCIL_TEST);
	this->renderData.enable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	/*
	 * Pre-GLES the alpha blend (second argument) was GL_MAX. GL_MAX is not
	 * supported in GLES2, so it was replaced with GL_FUNC_ADD. This might
	 * produce some artifacts and a more proper solution may have to be implemented.
	 * (IIRC GL_MAX was used to avoid borders around lit textures.)
	 */
	glBlendEquationSeparate(GL_FUNC_ADD, GL_FUNC_ADD);

	glClearStencil(0x00);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error initializing OpenGL: %u", error);
		return false;
	}

#if NOX_OPENGL_MULTISAMPLE
	if (this->lightMapMultisamplingSamples == 0)
	{
		this->renderData.disable(GL_MULTISAMPLE);
	}
	else
	{

		GLint maxMultisampleSamples = 0;
		glGetIntegerv(GL_MAX_SAMPLES, &maxMultisampleSamples);

		if ((int)this->lightMapMultisamplingSamples > maxMultisampleSamples)
		{
			this->lightMapMultisamplingSamples = static_cast<unsigned int>(maxMultisampleSamples);
		}

		if (this->lightMapMultisamplingSamples > 0)
		{
			this->renderData.enable(GL_MULTISAMPLE);
		}
		else
		{
			this->renderData.disable(GL_MULTISAMPLE);
		}
	}

	this->log.verbose().format("Enabling multisampling with %u samples.", this->lightMapMultisamplingSamples);
#endif

	this->log.verbose().format("Enabling %.1f times supersampling.", this->superSampleMultiplier);

	this->screenBuffer = FrameBuffer::getScreenBuffer();

	return true;
}

const TextureManager& OpenGlRenderer::getTextureManager() const
{
	return this->textureManager;
}

bool OpenGlRenderer::setupLitWorldRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string vertShaderName = "litGameplayPresent";
	std::string fragShaderName = "litGameplayPresent";

	if (supportGlMultiColorAttachments() == false)
	{
		fragShaderName = "litGameplayPresentNoLuminance";
	}

	const auto vertShader = this->createShader(resourceCache, shaderDirectory + vertShaderName + ".vert", GL_VERTEX_SHADER);
	const auto fragShader = this->createShader(resourceCache, shaderDirectory + fragShaderName + ".frag", GL_FRAGMENT_SHADER);
	assert(vertShader.isCompiled() && fragShader.isCompiled());

	this->litGameplayPresentProgram.create();

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;

	glBindAttribLocation(this->litGameplayPresentProgram.getId(), vertexCoordLocation, "vertex");
	glBindAttribLocation(this->litGameplayPresentProgram.getId(), textureCoordLocation, "textureCoord");

	// Link shader program.
	if (this->litGameplayPresentProgram.link(vertShader, fragShader) == false)
	{
		this->log.error().format("Could not link %s shader: %s", vertShaderName.c_str(), this->litGameplayPresentProgram.getLinkLog().c_str());
		return false;
	}

	this->litGameplayPresentRenderUniform = glGetUniformLocation(litGameplayPresentProgram.getId(), "renderTexture");
	assert(this->litGameplayPresentRenderUniform >= 0);

	this->litGameplayPresentLightUniform = glGetUniformLocation(litGameplayPresentProgram.getId(), "lightMapTexture");
	assert(this->litGameplayPresentLightUniform >= 0);

	if (supportGlMultiColorAttachments())
	{
		this->litGameplayPresentLightLuminanceUniform = glGetUniformLocation(litGameplayPresentProgram.getId(), "lightLuminanceTexture");
		assert(this->litGameplayPresentLightLuminanceUniform >= 0);
	}

	this->litGameplayPresentDayLightCoefficiencyUniform = glGetUniformLocation(litGameplayPresentProgram.getId(), "dayLightCoefficiency");
	assert(this->litGameplayPresentLightUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}

	this->renderData.bindShaderProgram(litGameplayPresentProgram);

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->renderTexture);
	glUniform1i(this->litGameplayPresentRenderUniform, 0);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->lightTexture);
	glUniform1i(this->litGameplayPresentLightUniform, 1);

	if (supportGlMultiColorAttachments())
	{
		glActiveTexture(GL_TEXTURE2);
		this->renderData.bindTexture(GL_TEXTURE_2D, this->lightLuminanceTexture);
		glUniform1i(this->litGameplayPresentLightLuminanceUniform, 2);
	}

	glActiveTexture(GL_TEXTURE0);

	return true;
}

bool OpenGlRenderer::setupEffectWorldRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	this->effectGameplayPresentProgram.create();

	const std::string SHADER_NAME = "effectGameplayPresent";

	const auto shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());


	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;

	glBindAttribLocation(this->effectGameplayPresentProgram.getId(), vertexCoordLocation, "vertex");
	glBindAttribLocation(this->effectGameplayPresentProgram.getId(), textureCoordLocation, "textureCoord");

	// Link shader program.
	if (this->effectGameplayPresentProgram.link(shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), this->effectGameplayPresentProgram.getLinkLog().c_str());
		return false;
	}

	this->effectGameplayPresentRenderUniform = glGetUniformLocation(effectGameplayPresentProgram.getId(), "renderTexture");
	assert(this->effectGameplayPresentRenderUniform >= 0);

	this->effectGameplayPresentLightUniform = glGetUniformLocation(effectGameplayPresentProgram.getId(), "effectMapTexture");
	assert(this->effectGameplayPresentLightUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}

	this->renderData.bindShaderProgram(effectGameplayPresentProgram);

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->litGameplayPresentTexture);
	glUniform1i(this->effectGameplayPresentRenderUniform, 0);

	glActiveTexture(GL_TEXTURE1);
	this->renderData.bindTexture(GL_TEXTURE_2D, this->effectMapTexture);
	glUniform1i(this->effectGameplayPresentLightUniform, 1);

	glActiveTexture(GL_TEXTURE0);

	return true;
}



bool OpenGlRenderer::setupSpriteRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;
	const GLuint colorLocation = 2;
	const GLuint lightLuminanceLocation = 3;

	{
		const std::string SHADER_NAME = "sprite";

		// Create shaders.
		GlslProgram spriteShaderProgram;
		spriteShaderProgram.create();

		const auto shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
		assert(shader.isValid());

		glBindAttribLocation(spriteShaderProgram.getId(), vertexCoordLocation, "vertex");
		glBindAttribLocation(spriteShaderProgram.getId(), textureCoordLocation, "textureCoord");
		glBindAttribLocation(spriteShaderProgram.getId(), colorLocation, "color");

		// Link shader program.
		if (spriteShaderProgram.link(shader.vertex, shader.fragment) == false)
		{
			this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), spriteShaderProgram.getLinkLog().c_str());
			return false;
		}

		this->renderData.bindShaderProgram(spriteShaderProgram);

		this->shaders[SHADER_NAME].shaderProgram = spriteShaderProgram;

		this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = glGetUniformLocation(spriteShaderProgram.getId(), "viewProjectionMatrix");;
		this->spriteColorMultiplierUniform = glGetUniformLocation(spriteShaderProgram.getId(), "colorMultiplier");;

		assert(this->spriteColorMultiplierUniform >= 0);
		assert(this->shaders[SHADER_NAME].ViewProjectionMatrixUniform >= 0);

		this->shaders[SHADER_NAME].TextureUniform = glGetUniformLocation(spriteShaderProgram.getId(), "renderTexture");
		assert(this->shaders[SHADER_NAME].TextureUniform >= 0);
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(this->shaders[SHADER_NAME].TextureUniform, 0);

		glUniform1f(this->spriteColorMultiplierUniform, 1.0f);
	}

	if (supportGlMultiColorAttachments())
	{
		const std::string SHADER_NAME = "spriteLightLuminance";

		// Create shaders.
		GlslProgram spriteShaderProgram;
		spriteShaderProgram.create();

		const auto shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
		assert(shader.isValid());

		// Bind shader attributes.
		const GLuint vertexCoordLocation = 0;
		const GLuint textureCoordLocation = 1;
		const GLuint colorLocation = 2;
		const GLuint lightLuminanceLocation = 3;

		glBindAttribLocation(spriteShaderProgram.getId(), vertexCoordLocation, "vertex");
		glBindAttribLocation(spriteShaderProgram.getId(), textureCoordLocation, "textureCoord");
		glBindAttribLocation(spriteShaderProgram.getId(), colorLocation, "color");
		glBindAttribLocation(spriteShaderProgram.getId(), lightLuminanceLocation, "lightLuminance");

		// Link shader program.
		if (spriteShaderProgram.link(shader.vertex, shader.fragment) == false)
		{
			this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), spriteShaderProgram.getLinkLog().c_str());
			return false;
		}

		this->renderData.bindShaderProgram(spriteShaderProgram);

		this->shaders[SHADER_NAME].shaderProgram = spriteShaderProgram;

		this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = glGetUniformLocation(spriteShaderProgram.getId(), "viewProjectionMatrix");;
		this->spriteLightLuminanceColorMultiplierUniform = glGetUniformLocation(spriteShaderProgram.getId(), "colorMultiplier");;

		assert(this->spriteLightLuminanceColorMultiplierUniform >= 0);
		assert(this->shaders[SHADER_NAME].ViewProjectionMatrixUniform >= 0);

		this->shaders[SHADER_NAME].TextureUniform = glGetUniformLocation(spriteShaderProgram.getId(), "renderTexture");
		assert(this->shaders[SHADER_NAME].TextureUniform >= 0);
		glActiveTexture(GL_TEXTURE0);
		glUniform1i(this->shaders[SHADER_NAME].TextureUniform, 0);

		glUniform1f(this->spriteLightLuminanceColorMultiplierUniform, 1.0f);
	}

	this->textureRenderer.init(this->context, this->renderData, vertexCoordLocation, textureCoordLocation, colorLocation, lightLuminanceLocation);
	this->textureRenderer.enableCull(true);

	return true;
}

bool OpenGlRenderer::setupDebugRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "debug";

	// Create shaders.
	GlslProgram debugShaderProgram;
	debugShaderProgram.create();

	const auto shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(debugShaderProgram.getId(), vertexLocation, "vertex");
	glBindAttribLocation(debugShaderProgram.getId(), colorLocation, "color");

	// Link shader program.
	if (debugShaderProgram.link(shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), debugShaderProgram.getLinkLog().c_str());
		return false;
	}


	this->renderData.bindShaderProgram(debugShaderProgram);
	this->shaders[SHADER_NAME].shaderProgram = debugShaderProgram;
	this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = glGetUniformLocation(debugShaderProgram.getId(), "modelViewProjectionMatrix");
	assert(this->shaders[SHADER_NAME].ViewProjectionMatrixUniform >= 0);


    this->debugRenderer->init(this->renderData, this->shaders[SHADER_NAME]);

	return true;
}

void OpenGlRenderer::addDebugGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	std::lock_guard<std::mutex> debugLock(this->debugRenderMutex);
	this->debugRenderer->addGeometrySet(set);
}

void OpenGlRenderer::removeDebugGeometrySet(const std::shared_ptr<GeometrySet>& set)
{
	std::lock_guard<std::mutex> debugLock(this->debugRenderMutex);
	this->debugRenderer->removeGeometrySet(set);
}

void OpenGlRenderer::addLight(std::shared_ptr<Light> light)
{
	std::lock_guard<std::mutex> lightLock(this->lightRenderMutex);

	if (light->isEffect() == true)
	{
		this->lightEffects.push_back(light);
		this->effectLightsUpdated = true;
	}
	else
	{
		if (light->renderHint == Light::RenderHint::DYNAMIC)
		{
			this->dynamicLights.push_back(light);
			this->dynamicLightsUpdated = true;
		}
		else if (light->renderHint == Light::RenderHint::STATIC)
		{
			this->staticLights.push_back(light);
			this->staticLightsUpdated = true;
		}
	}
}

void OpenGlRenderer::removeLight(std::shared_ptr<Light> light)
{
	std::lock_guard<std::mutex> lightLock(this->lightRenderMutex);


	if (light->isEffect() == true)
	{
		this->effectLightsUpdated = util::removeFast(this->lightEffects, light);
	}
	else
	{
		if (light->renderHint == Light::RenderHint::DYNAMIC)
		{
			this->dynamicLightsUpdated = util::removeFast(this->dynamicLights, light);
		}
		else if (light->renderHint == Light::RenderHint::STATIC)
		{
			this->staticLightsUpdated = util::removeFast(this->staticLights, light);
		}
	}
}

void OpenGlRenderer::resizeWindow(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	glViewport(0, 0, (GLsizei)this->windowSize.x, (GLsizei)this->windowSize.y);
	this->windowSizeChanged = true;

	deleteTextureIfValid(this->renderTexture);
	deleteTextureIfValid(this->lightLuminanceTexture);
	deleteTextureIfValid(this->effectMapTexture);
	deleteTextureIfValid(this->lightMapTexture);
	deleteTextureIfValid(this->lightHorizontalBlurTexture);
	deleteTextureIfValid(this->lightVerticalBlurTexture);
	deleteTextureIfValid(this->litGameplayPresentTexture);

	const glm::vec2 windowSizeFloat(this->windowSize.x, this->windowSize.y);

	glm::ivec2 supersampledSize;
	supersampledSize.x = static_cast<int>(windowSizeFloat.x * this->superSampleMultiplier);
	supersampledSize.y = static_cast<int>(windowSizeFloat.y * this->superSampleMultiplier);

	this->renderTexture = createTexture(this->glState, supersampledSize.x, supersampledSize.y);
	auto gameplayTextures = std::vector<GLuint>{this->renderTexture};

	if (supportGlMultiColorAttachments())
	{
		this->lightLuminanceTexture = createTexture(this->glState, supersampledSize.x, supersampledSize.y, GL_RGB, GL_RGB);
		gameplayTextures.push_back(this->lightLuminanceTexture);
	}

	this->gameplayFBO = createFrameBufferObject(supersampledSize.x, supersampledSize.y, gameplayTextures, true);

	const GLsizei effectTextureWitdh = glm::max(1, static_cast<GLsizei>(windowSizeFloat.x * this->effectTextureSizeMultiplier));
	const GLsizei effectTextureHeight = glm::max(1, static_cast<GLsizei>(windowSizeFloat.y * this->effectTextureSizeMultiplier));

	const GLsizei lightTextureWidth = glm::max(1, static_cast<GLsizei>(windowSizeFloat.x * this->lightTextureSizeMultiplier));
	const GLsizei lightTextureHeight = glm::max(1, static_cast<GLsizei>(windowSizeFloat.y * this->lightTextureSizeMultiplier));

	this->effectMapTexture = createTexture(this->glState, effectTextureWitdh, effectTextureHeight);
	this->effectMapFBO = createFrameBufferObject(effectTextureWitdh, effectTextureHeight, {this->effectMapTexture});

	this->lightMapTexture = createTexture(this->glState, lightTextureWidth, lightTextureHeight);
	this->lightMapFBO = createFrameBufferObject(lightTextureWidth, lightTextureHeight, {this->lightMapTexture});

	this->lightHorizontalBlurTexture = createTexture(this->glState, lightTextureWidth, lightTextureHeight);
	this->lightHorizontalBlurFBO = createFrameBufferObject(lightTextureWidth, lightTextureHeight, {this->lightHorizontalBlurTexture});

	this->lightVerticalBlurTexture = createTexture(this->glState, lightTextureWidth, lightTextureHeight);
	this->lightVerticalBlurFBO = createFrameBufferObject(lightTextureWidth, lightTextureHeight, {this->lightVerticalBlurTexture});

	this->litGameplayPresentTexture = createTexture(this->glState, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
	this->litGameplayPresentFBO = createFrameBufferObject(static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y), {this->litGameplayPresentTexture}, true);

	if (this->blurEnabled == true)
	{
		this->lightTexture = this->lightVerticalBlurTexture;
	}
	else
	{
		this->lightTexture = this->lightMapTexture;
	}

#if NOX_OPENGL_MULTISAMPLE
	if (this->lightMapMultisamplingSamples > 0)
	{
		this->lightMapMultiSampleFBO = createMultisampleFrameBufferObject(static_cast<GLsizei>(lightTextureWidth), static_cast<GLsizei>(lightTextureHeight), std::vector<GLuint>(), static_cast<GLsizei>(this->lightMapMultisamplingSamples));
	}
#endif

	for (auto& subRenderer : this->subRenderers)
	{
		subRenderer->onScreenSizeChange(this->windowSize);
	}

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Got error %d while resizing.", error);
	}
	else
	{
		this->log.debug().format("Resized renderer to %d %d", this->windowSize.x, this->windowSize.y);
	}
}

SubRenderer* OpenGlRenderer::addSubRenderer(std::unique_ptr<SubRenderer> renderer)
{
	SubRenderer* subRenderer = renderer.get();
	this->subRenderers.push_back(std::move(renderer));

	if (this->initialized == true)
	{
		subRenderer->init(this->context, this->renderData, this->windowSize);
	}

	return subRenderer;
}

void OpenGlRenderer::removeSubRenderer(SubRenderer* renderer)
{
	auto renderIt = std::find_if(
			this->subRenderers.begin(),
			this->subRenderers.end(),
			[renderer] (const std::unique_ptr<SubRenderer>& renderToCheck)
			{
				return (renderer == renderToCheck.get());
			}
	);

	if (renderIt != this->subRenderers.end())
	{
		this->subRenderers.erase(renderIt);
	}
}

void OpenGlRenderer::addStenciledTiledTextureLayer(
	const std::string& layerName,
	std::unique_ptr<StenciledTiledTextureGenerator> generator,
	std::unique_ptr<StenciledTiledTextureRenderer> renderer,
	unsigned int renderLevel)
{
	const auto layerIt = this->stenciledTiledTextureLayers.find(layerName);

	if (layerIt == this->stenciledTiledTextureLayers.end())
	{
		StenciledTiledTextureLayer& layer = this->stenciledTiledTextureLayers[layerName];
		layer.generator = std::move(generator);
		layer.renderer = std::move(renderer);
		layer.renderLevel = renderLevel;

		layer.renderer->setup(this->renderData);
	}
}

void OpenGlRenderer::removeTiledTextureLayer(const std::string& layerName)
{
	this->stenciledTiledTextureLayers.erase(layerName);
}

void OpenGlRenderer::setBackgroundGradient(std::unique_ptr<BackgroundGradient> background)
{
	this->backgroundGradient = std::move(background);

	if (this->backgroundGradient != nullptr)
	{
		const auto& shaderProgram = this->shaders["backgroundGradient"].shaderProgram;

		if (shaderProgram.isLinked() == false)
		{
			this->setupBackgroundGradientRendering(this->context->getResourceAccess(), this->shaderDirectory);
		}

		this->backgroundGradient->setupRendering(this->renderData, shaderProgram.getId());
	}
}

void OpenGlRenderer::lightUpdate(const std::shared_ptr<Light>& light)
{
	if (light->isEffect() == true)
	{
		this->effectLightsUpdated = true;
	}
	else
	{
		if (light->renderHint == Light::RenderHint::DYNAMIC)
		{
			this->dynamicLightsUpdated = true;
		}
		else if (light->renderHint == Light::RenderHint::STATIC)
		{
			this->staticLightsUpdated = true;
		}
	}
}

void OpenGlRenderer::setAmbientLightLevel(const float lightLevel)
{
	this->lightLevel = lightLevel;
}

bool OpenGlRenderer::setupLightMapRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "lightMap";

	GlslProgram lightMapShaderProgram;
	lightMapShaderProgram.create();

	const auto shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(lightMapShaderProgram.getId(), vertexLocation, "vertex");
	glBindAttribLocation(lightMapShaderProgram.getId(), colorLocation, "color");

	// Link shader program.
	if (lightMapShaderProgram.link(shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), lightMapShaderProgram.getLinkLog().c_str());
		return false;
	}

	GLint matrixUniformLocation = glGetUniformLocation(lightMapShaderProgram.getId(), "modelViewProjectionMatrix");
	assert(matrixUniformLocation >= 0);
	this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = matrixUniformLocation;

	this->shaders[SHADER_NAME].shaderProgram = lightMapShaderProgram;

	this->lightMapModel = GlVertexModel(&this->glState);

	this->lightMapAttributes = this->lightMapModel.createPackedVertexAttributes(
			GL_STREAM_DRAW,
			ObjectCoordinate::getVertexAttributeDefs(vertexLocation, colorLocation));

	return true;
}

bool OpenGlRenderer::setupEffectMapRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "effectMap";

	// Create shaders.2
	GlslProgram effectMapShaderProgram;
	effectMapShaderProgram.create();

	const auto shader = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + SHADER_NAME);
	assert(shader.isValid());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(effectMapShaderProgram.getId(), vertexLocation, "vertex");
	glBindAttribLocation(effectMapShaderProgram.getId(), colorLocation, "color");

	// Link shader program.
	if (effectMapShaderProgram.link(shader.vertex, shader.fragment) == false)
	{
		this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), effectMapShaderProgram.getLinkLog().c_str());
		return false;
	}

	GLint matrixUniformLocation = glGetUniformLocation(effectMapShaderProgram.getId(), "modelViewProjectionMatrix");
	assert(matrixUniformLocation >= 0);
	this->shaders[SHADER_NAME].ViewProjectionMatrixUniform = matrixUniformLocation;

	this->shaders[SHADER_NAME].shaderProgram = effectMapShaderProgram;

	this->effectMapModel = GlVertexModel(&this->glState);

	this->effectMapAttributes = this->effectMapModel.createPackedVertexAttributes(
			GL_STREAM_DRAW,
			ObjectCoordinate::getVertexAttributeDefs(vertexLocation, colorLocation));

	return true;
}

bool OpenGlRenderer::setupLightHorizontalBlur(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "horizontalBlurCoordCalculation";

	// Create shaders.
	this->lightHorizontalBlurProgram.create();

	const auto vertexShader = this->createShader(resourceCache, shaderDirectory + SHADER_NAME + ".vert", GL_VERTEX_SHADER);
	assert(vertexShader.isCompiled());

	const auto fragmentShader = this->createShader(resourceCache, shaderDirectory + "gaussianBlurShader.frag", GL_FRAGMENT_SHADER);
	assert(fragmentShader.isCompiled());

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;

	glBindAttribLocation(lightHorizontalBlurProgram.getId(), vertexCoordLocation, "vertex");
	glBindAttribLocation(lightHorizontalBlurProgram.getId(), textureCoordLocation, "textureCoord");

	// Link shader program.
	if (lightHorizontalBlurProgram.link(vertexShader, fragmentShader) == false)
	{
		this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), this->lightHorizontalBlurProgram.getLinkLog().c_str());
		return false;
	}

	this->lightHorizontalBlurTextureUniform = glGetUniformLocation(lightHorizontalBlurProgram.getId(), "renderTexture");
	assert(this->lightHorizontalBlurTextureUniform >= 0);

	this->lightHorizontalBlurSizeUniform = glGetUniformLocation(lightHorizontalBlurProgram.getId(), "blurSize");
	assert(this->lightHorizontalBlurSizeUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}

	this->renderData.bindShaderProgram(lightHorizontalBlurProgram.getId());

	float blurSize = this->pixelsPerBlurStep / (static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier);
	glUniform1f(this->lightHorizontalBlurSizeUniform, blurSize);

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, lightMapTexture);
	glUniform1i(this->lightHorizontalBlurTextureUniform, 0);

	return true;
}

bool OpenGlRenderer::setupLightVerticalBlur(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string SHADER_NAME = "verticalBlurCoordCalculation";

	// Create shaders.
	this->lightVerticalBlurProgram.create();

	const auto vertexShader = this->createShader(resourceCache, shaderDirectory + SHADER_NAME + ".vert", GL_VERTEX_SHADER);
	assert(vertexShader.isCompiled());

	const auto fragmentShader = this->createShader(resourceCache, shaderDirectory + "gaussianBlurShader.frag", GL_FRAGMENT_SHADER);
	assert(fragmentShader.isCompiled());

	// Bind shader attributes.
	const GLuint vertexCoordLocation = 0;
	const GLuint textureCoordLocation = 1;

	glBindAttribLocation(lightVerticalBlurProgram.getId(), vertexCoordLocation, "vertex");
	glBindAttribLocation(lightVerticalBlurProgram.getId(), textureCoordLocation, "textureCoord");

	// Link shader program.
	if (lightVerticalBlurProgram.link(vertexShader, fragmentShader) == false)
	{
		this->log.error().format("Could not link %s shader: %s", SHADER_NAME.c_str(), this->lightVerticalBlurProgram.getLinkLog().c_str());
		return false;
	}

	this->lightVerticalBlurTextureUniform = glGetUniformLocation(lightVerticalBlurProgram.getId(), "renderTexture");
	assert(this->lightVerticalBlurTextureUniform >= 0);

	this->lightVerticalBlurSizeUniform = glGetUniformLocation(lightVerticalBlurProgram.getId(), "blurSize");
	assert(this->lightVerticalBlurSizeUniform >= 0);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error = %d", error);
	}

	this->renderData.bindShaderProgram(lightVerticalBlurProgram);

	float blurSize = this->pixelsPerBlurStep / (static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier);
	glUniform1f(this->lightVerticalBlurSizeUniform, blurSize);

	glActiveTexture(GL_TEXTURE0);
	this->renderData.bindTexture(GL_TEXTURE_2D, lightHorizontalBlurTexture);
	glUniform1i(this->lightVerticalBlurTextureUniform, 0);

	return true;
}

bool OpenGlRenderer::setupBackgroundGradientRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory)
{
	const std::string vertShaderName = "backgroundGradient";
	std::string fragShaderName = "backgroundGradient";

	if (supportGlMultiColorAttachments() == false)
	{
		fragShaderName = fragShaderName + "NoLuminance";
	}

	const auto vertShader = this->createShader(resourceCache, shaderDirectory + vertShaderName + ".vert", GL_VERTEX_SHADER);
	const auto fragShader = this->createShader(resourceCache, shaderDirectory + fragShaderName + ".frag", GL_FRAGMENT_SHADER);
	assert(vertShader.isCompiled() && fragShader.isCompiled());

	auto shaderProgram = GlslProgram();
	shaderProgram.create();
	assert(shaderProgram.isCreated());

	// Bind shader attributes.
	GLuint vertexLocation = 0;
	GLuint colorLocation = 1;
	glBindAttribLocation(shaderProgram.getId(), vertexLocation, "vertex");
	glBindAttribLocation(shaderProgram.getId(), colorLocation, "color");

	// Link shader program.
	if (shaderProgram.link(vertShader, fragShader) == false)
	{
		this->log.error().format("Could not link %s shader: %s", vertShaderName.c_str(), shaderProgram.getLinkLog().c_str());
		return false;
	}

	this->shaders[vertShaderName].shaderProgram = shaderProgram;

	return true;
}


void OpenGlRenderer::onLightMapRender()
{
	if (lightMapMultisamplingSamples > 0)
	{
		this->lightMapMultiSampleFBO.bind(GL_FRAMEBUFFER);
	}
	else
	{
		this->lightMapFBO.bind(GL_FRAMEBUFFER);
	}

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(lightClearColor.r, lightClearColor.g, lightClearColor.b, lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	const GLsizei lightTextureWidth(static_cast<GLsizei>(static_cast<float>(this->windowSize.x) * this->lightTextureSizeMultiplier));
	const GLsizei lightTextureHeight(static_cast<GLsizei>(static_cast<float>(this->windowSize.y) * this->lightTextureSizeMultiplier));

	if (this->lightCoords.size() > 0)
	{
		this->renderData.disable(GL_STENCIL_TEST);

		const auto& shaderProgram = this->shaders["lightMap"].shaderProgram;
		this->renderData.bindShaderProgram(shaderProgram);

		glUniformMatrix4fv(this->shaders["lightMap"].ViewProjectionMatrixUniform,
			1, GL_FALSE, glm::value_ptr(this->renderCamera->getViewProjectionMatrix()));

		if (this->lightTextureSizeMultiplier != 1.0f)
		{
			// We need to set the viewport to fit the new resolution.
			glViewport(0, 0, lightTextureWidth, lightTextureHeight);
		}

		this->lightMapModel.draw(GL_TRIANGLES);

		if (this->lightTextureSizeMultiplier != 1.0f)
		{
			glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
		}
	}

#if NOX_OPENGL_SUPPORT_MULTISAMPLE
	if (lightMapMultisamplingSamples > 0)
	{
		this->lightMapMultiSampleFBO.bind(GL_READ_FRAMEBUFFER);
		this->lightMapFBO.bind(GL_DRAW_FRAMEBUFFER);
		glBlitFramebuffer(0, 0,
				  lightTextureWidth,
				  lightTextureHeight,
				  0, 0,
				  lightTextureWidth,
				  lightTextureHeight,
				  GL_COLOR_BUFFER_BIT, GL_LINEAR);
	}
#endif

	if (this->blurEnabled == true)
	{

#ifdef TIME_BLUR
		if (this->frameCount >= NUM_AVERAGING_FRAMES)
		{
			auto nanosecondsTotal = std::chrono::duration_cast<std::chrono::nanoseconds>(this->accumulatedBlurTime).count();
			auto nanosecondsPerFrame = nanosecondsTotal / this->frameCount;

			auto microsecondsTotal = std::chrono::duration_cast<std::chrono::microseconds>(this->accumulatedBlurTime).count();
			auto microsecondsPerFrame = microsecondsTotal / this->frameCount;

			auto millisecondsTotal = std::chrono::duration_cast<std::chrono::milliseconds>(this->accumulatedBlurTime).count();
			auto millisecondsPerFrame = millisecondsTotal / this->frameCount;

			this->log.debug().format("Blur rendering used %i nanoseconds per frame.", nanosecondsPerFrame);
			this->log.debug().format("Blur rendering used %i microseconds per frame.", microsecondsPerFrame);
			this->log.debug().format("Blur rendering used %i milliseconds per frame.", millisecondsPerFrame);

			this->frameCount = 0;
			this->accumulatedBlurTime = std::chrono::high_resolution_clock::duration(0);
		}

		auto startTime = std::chrono::high_resolution_clock::now();
#endif


		///////////// INSERT BLUR ////////
		this->renderData.disable(GL_BLEND);
		this->renderLightHorizontalBlur();
		this->renderLightVerticalBlur();
		this->renderData.enable(GL_BLEND);
		//////////////////////////////////

#ifdef TIME_BLUR
		auto endTime = std::chrono::high_resolution_clock::now();
		this->accumulatedBlurTime += (endTime - startTime);
		this->frameCount++;
#endif
	}
}

void OpenGlRenderer::onEffectMapRender()
{
	this->effectMapFBO.bind(GL_FRAMEBUFFER);

	if (this->lightClearColor != this->currentClearColor)
	{
		glClearColor(lightClearColor.r, lightClearColor.g, lightClearColor.b, lightClearColor.a);
		this->currentClearColor = this->lightClearColor;
	}
	glClear(GL_COLOR_BUFFER_BIT);

	if (this->effectCoords.size() > 0)
	{
		renderData.disable(GL_STENCIL_TEST);

		const auto& shaderProgram = this->shaders["effectMap"].shaderProgram;
		this->renderData.bindShaderProgram(shaderProgram);

		glUniformMatrix4fv(this->shaders["effectMap"].ViewProjectionMatrixUniform,
				   1, GL_FALSE, glm::value_ptr(this->renderCamera->getViewProjectionMatrix()));

		if (this->effectTextureSizeMultiplier != 1.0f)
		{
			const GLsizei effectTextureWitdh(static_cast<GLsizei>(static_cast<float>(this->windowSize.x) * this->effectTextureSizeMultiplier));
			const GLsizei effectTextureHeight(static_cast<GLsizei>(static_cast<float>(this->windowSize.y) * this->effectTextureSizeMultiplier));

			// We need to set the viewport to fit the new resolution.
			glViewport(0, 0, effectTextureWitdh, effectTextureHeight);
		}

		this->effectMapModel.draw(GL_TRIANGLES);

		if (this->effectTextureSizeMultiplier != 1.0f)
		{
			glViewport(0, 0, static_cast<GLsizei>(this->windowSize.x), static_cast<GLsizei>(this->windowSize.y));
		}
	}
}

TextureRenderer& OpenGlRenderer::getTextureRenderer()
{
	return this->textureRenderer;
}


bool OpenGlRenderer::isDebugRenderingEnabled()
{
	return this->debugRenderingEnabled;
}


void OpenGlRenderer::lightRenderingDataPrepare()
{
	std::lock_guard<std::mutex> lightLock(this->lightRenderMutex);

	auto anyLightsUpdated = false;
	if (this->dynamicLightsUpdated == true || this->staticLightsUpdated == true)
	{
		anyLightsUpdated = true;
	}

	auto onlyDynamicLightsUpdated = false;
	if (this->dynamicLightsUpdated == true && this->staticLightsUpdated == false)
	{
		onlyDynamicLightsUpdated = true;
	}

	if (this->staticLightsUpdated == true)
	{

		// For efficient lightCoords insertion, the whole vector will be repopulated. Thus dynamicLights must be updated.
		// This shouldn't happen too often since static lights aren't supposed to move (if used properly).
		// The only time where this will happen is when static lights are added or removed.
		this->dynamicLightsUpdated = true;
		this->lightCoords.clear();

		this->numberOfStaticLightCoords = 0;

		for (const std::shared_ptr<Light>& light : this->staticLights)
		{
			if (light != nullptr && light->isEnabled() == true)
			{
				GeometrySet& geometrySet = light->lightArea;

				if (geometrySet.isActive() == true && geometrySet.getGeometryPrimitive() == GeometryPrimitive::TRIANGLE)
				{
					unsigned int renderDataSize = 0;

					const std::vector<ObjectCoordinate>& setCoordinates = geometrySet.getVertexData();

					if (geometrySet.getDataMode() == GeometrySet::DataMode::REQUEST)
					{
						renderDataSize = geometrySet.updateRenderData();
					}
					else
					{
						renderDataSize = static_cast<unsigned int>(setCoordinates.size());
					}

					this->numberOfStaticLightCoords += renderDataSize;

					auto insertFromBeginIt = setCoordinates.begin();

					auto insertFromEndIt = insertFromBeginIt;
					std::advance(insertFromEndIt, renderDataSize);

					this->lightCoords.insert(this->lightCoords.end(), insertFromBeginIt, insertFromEndIt);
				}
			}
		}

		this->staticLightsUpdated = false;
	}

	if (this->dynamicLightsUpdated == true)
	{
		// When dynamic lights are updated, but not static lights, we have to erase all the dynamic lights from the lightCoords vector.
		// If both are updated the vector has been cleared by the static light update, so we don't need to erase.
		if (onlyDynamicLightsUpdated == true)
		{
			auto eraseStartIt = this->lightCoords.begin();
			std::advance(eraseStartIt, this->numberOfStaticLightCoords);

			auto eraseEndIt = this->lightCoords.end();

			this->lightCoords.erase(eraseStartIt, eraseEndIt);
		}

		for (const std::shared_ptr<Light>& light : this->dynamicLights)
		{
			if (light != nullptr && light->isEnabled() == true)
			{
				GeometrySet& geometrySet = light->lightArea;

				if (geometrySet.isActive() == true && geometrySet.getGeometryPrimitive() == GeometryPrimitive::TRIANGLE)
				{
					unsigned int renderDataSize = 0;

					const std::vector<ObjectCoordinate>& setCoordinates = geometrySet.getVertexData();

					if (geometrySet.getDataMode() == GeometrySet::DataMode::REQUEST)
					{
						renderDataSize = geometrySet.updateRenderData();
					}
					else
					{
						renderDataSize = static_cast<unsigned int>(setCoordinates.size());
					}

					auto insertFromBeginIt = setCoordinates.begin();

					auto insertFromEndIt = insertFromBeginIt;
					std::advance(insertFromEndIt, renderDataSize);

					this->lightCoords.insert(this->lightCoords.end(), insertFromBeginIt, insertFromEndIt);
				}
			}
		}

		this->dynamicLightsUpdated = false;
	}

	if (anyLightsUpdated)
	{
		this->lightMapModel.setNumVertices(this->lightCoords.size());
		this->lightCoordsUpdated = true;
	}
}

void OpenGlRenderer::lightRenderingIo()
{
	if (this->lightCoordsUpdated == true && this->lightCoords.size() > 0)
	{
		this->lightMapAttributes->getBuffer().overwriteData(this->lightCoords, &this->glState, this->asyncBufferUploader);
		this->lightCoordsUpdated = false;
	}
}

void OpenGlRenderer::effectRenderingDataPrepare()
{
	std::lock_guard<std::mutex> lightLock(this->lightRenderMutex);

	if (this->effectLightsUpdated == true)
	{
		this->effectCoords.clear();

		for (const std::shared_ptr<Light>& light : this->lightEffects)
		{
			if (light != nullptr && light->isEnabled() == true)
			{
				GeometrySet& geometrySet = light->lightArea;

				if (geometrySet.isActive() == true && geometrySet.getGeometryPrimitive() == GeometryPrimitive::TRIANGLE)
				{
					unsigned int renderDataSize = 0;

					const std::vector<ObjectCoordinate>& setCoordinates = geometrySet.getVertexData();

					if (geometrySet.getDataMode() == GeometrySet::DataMode::REQUEST)
					{
						renderDataSize = geometrySet.updateRenderData();
					}
					else
					{
						renderDataSize = static_cast<unsigned int>(setCoordinates.size());
					}

					auto insertFromBeginIt = setCoordinates.begin();

					auto insertFromEndIt = insertFromBeginIt;
					std::advance(insertFromEndIt, renderDataSize);

					this->effectCoords.insert(this->effectCoords.end(), insertFromBeginIt, insertFromEndIt);
				}
			}
		}

		this->effectMapModel.setNumVertices(this->effectCoords.size());

		this->effectLightsUpdated = false;
		this->effectCoordsUpdated = true;
	}
}

void OpenGlRenderer::effectRenderingIo()
{
	if (this->effectCoordsUpdated == true && this->effectCoords.size() > 0)
	{
		this->effectMapAttributes->getBuffer().overwriteData(this->effectCoords, &this->glState, this->asyncBufferUploader);
		this->effectCoordsUpdated = false;
	}
}

void OpenGlRenderer::debugDataPreparation()
{
	std::unique_lock<std::mutex> debugLock(this->debugRenderMutex, std::defer_lock);

	while (!this->quitDebugDataPreparation)
	{
		this->debugDataPreparationStartBarrier.wait();
		debugLock.lock();
		this->debugRenderer->prepareData();
		debugLock.unlock();
		this->debugDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::textureDataPreparation()
{
	while (!this->quitTextureDataPreparation)
	{
		this->textureDataPreparationStartBarrier.wait();
		this->parseSceneGraph();
		this->textureDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::lightingDataPreparation()
{
	while (!this->quitLightingDataPreparation)
	{
		this->lightingDataPreparationStartBarrier.wait();
		this->lightRenderingDataPrepare();
		this->lightingDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::effectDataPreparation()
{
	while (!this->quitEffectDataPreparation)
	{
		this->effectDataPreparationStartBarrier.wait();
		this->effectRenderingDataPrepare();
		this->effectDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::setLightStartRenderLevel(const unsigned int litRenderLevel)
{
	this->lightStartRenderLevel = litRenderLevel;
}

void OpenGlRenderer::subRenderersDataPreparation()
{
	while (!this->quitSubRenderersDataPreparation)
	{
		this->subRenderersDataPreparationStartBarrier.wait();
		for (auto& subRenderer : this->subRenderers)
		{
			subRenderer->prepareRendering(this->renderData);
		}
		this->subRenderersDataPreparationFinishedBarrier.wait();
	}
}

void OpenGlRenderer::organizeRenderSteps()
{
	this->renderSteps.clear();

	std::multimap<unsigned int, StenciledTiledTextureRenderer*> sortedStenciledTextureLayers;
	for (const auto& tileLayer : this->stenciledTiledTextureLayers)
	{
		sortedStenciledTextureLayers.insert({tileLayer.second.renderLevel, tileLayer.second.renderer.get()});
	}

	ShaderInfo litTextureShader = this->shaders["spriteLightLuminance"];
	if (supportGlMultiColorAttachments() == false)
	{
		litTextureShader = this->shaders["sprite"];
	}

	const ShaderInfo& textureShader = this->shaders["sprite"];

	std::vector<std::unique_ptr<StenciledTiledTextureStencilStep>> stencilRenderSteps;

	unsigned int previousLayer = 0;
	GLint currentStencilRef = 1;

	if (this->backgroundGradient != nullptr)
	{
		std::unique_ptr<BackgroundGradientRenderStep> backgroundStep(new BackgroundGradientRenderStep(
			this->backgroundGradient.get(),
			previousLayer,
			currentStencilRef));

		this->renderSteps.push_back(std::move(backgroundStep));
	}

	for (const auto& layer : sortedStenciledTextureLayers)
	{
		const unsigned int currentLayer = layer.first;

		if (currentLayer > previousLayer && previousLayer < this->lightStartRenderLevel)
		{
			std::unique_ptr<TextureRenderStep> textureStep(new TextureRenderStep(
				this->textureRenderer,
				previousLayer,
				this->lightStartRenderLevel - 1u,
				textureShader.shaderProgram.getId(),
				currentStencilRef,
				false));

			this->renderSteps.push_back(std::move(textureStep));

			previousLayer = this->lightStartRenderLevel;
		}

		if (currentLayer > previousLayer)
		{
			std::unique_ptr<TextureRenderStep> litTextureStep(new TextureRenderStep(
				this->textureRenderer,
				previousLayer,
				currentLayer - 1u,
				litTextureShader.shaderProgram.getId(),
				currentStencilRef,
				true));

			this->renderSteps.push_back(std::move(litTextureStep));
		}

		std::unique_ptr<StenciledTiledTextureStencilStep> tiledStencilStep(new StenciledTiledTextureStencilStep(
			layer.second,
			layer.first,
			currentStencilRef));

		stencilRenderSteps.push_back(std::move(tiledStencilStep));

		std::unique_ptr<StenciledTiledTextureRenderStep> tiledRenderStep(new StenciledTiledTextureRenderStep(
			layer.second,
			layer.first,
			litTextureShader.shaderProgram.getId(),
			currentStencilRef));

		this->renderSteps.push_back(std::move(tiledRenderStep));

		currentStencilRef += 1;

		previousLayer = currentLayer;
	}

	const unsigned int lastRenderLevel = this->NUM_RENDER_LEVELS - 1;

	if (previousLayer < lastRenderLevel && previousLayer < this->lightStartRenderLevel)
	{
		std::unique_ptr<TextureRenderStep> textureStep(new TextureRenderStep(
			this->textureRenderer,
			previousLayer,
			this->lightStartRenderLevel - 1u,
			textureShader.shaderProgram.getId(),
			currentStencilRef,
			false));

		this->renderSteps.push_back(std::move(textureStep));

		previousLayer = this->lightStartRenderLevel;
	}

	if (previousLayer < lastRenderLevel)
	{
		std::unique_ptr<TextureRenderStep> litTextureStep(new TextureRenderStep(
			this->textureRenderer,
			previousLayer,
			lastRenderLevel,
			litTextureShader.shaderProgram.getId(),
			currentStencilRef,
			true));

		this->renderSteps.push_back(std::move(litTextureStep));
	}

	// Do the stencil operations before any rendering.
	this->renderSteps.insert(
		this->renderSteps.begin(),
		std::make_move_iterator(stencilRenderSteps.begin()),
		std::make_move_iterator(stencilRenderSteps.end()));
}

OpenGlRenderer::RenderStep::RenderStep(const unsigned int startLevel, const unsigned int endLevel):
	startLevel(startLevel),
	endLevel(endLevel)
{
}

OpenGlRenderer::RenderStep::~RenderStep() = default;

unsigned int OpenGlRenderer::RenderStep::getStartRenderLevel() const
{
	return this->startLevel;
}

unsigned int OpenGlRenderer::RenderStep::getEndRenderLevel() const
{
	return this->endLevel;
}

OpenGlRenderer::TextureRenderStep::TextureRenderStep(
		TextureRenderer& renderer,
		const unsigned int startLevel,
		const unsigned int endLevel,
		const GLuint shader,
		const GLint stencilRef,
		const bool affectedByLight):
	RenderStep(startLevel, endLevel),
	renderer(renderer),
	shader(shader),
	stencilRef(stencilRef),
	affectedByLight(affectedByLight)
{
}

void OpenGlRenderer::TextureRenderStep::render(RenderData& renderData, const glm::mat4& /*viewProjectionMatrix*/)
{
	renderData.enable(GL_STENCIL_TEST);
	renderData.setStencilMask(0x00);
	renderData.setStencilFunc(GL_GREATER, this->stencilRef, 0xFF);

	renderData.bindShaderProgram(this->shader);
	this->renderer.drawData(renderData, this->getStartRenderLevel(), this->getEndRenderLevel());
}

OpenGlRenderer::StenciledTiledTextureStencilStep::StenciledTiledTextureStencilStep(StenciledTiledTextureRenderer* renderer, const unsigned int renderLevel, const GLint ref) :
	RenderStep(renderLevel, renderLevel),
	renderer(renderer),
	ref(ref)
{
}

void OpenGlRenderer::StenciledTiledTextureStencilStep::render(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	assert(this->renderer != nullptr);

	this->renderer->stencil(renderData, viewProjectionMatrix, this->ref);
}

OpenGlRenderer::StenciledTiledTextureRenderStep::StenciledTiledTextureRenderStep(StenciledTiledTextureRenderer* renderer, const unsigned int renderLevel, const GLuint shader, const GLint ref):
	RenderStep(renderLevel, renderLevel),
	renderer(renderer),
	shader(shader),
	ref(ref)
{
}

void OpenGlRenderer::StenciledTiledTextureRenderStep::render(RenderData& renderData, const glm::mat4& viewProjectionMatrix)
{
	assert(this->renderer != nullptr);

	renderData.bindShaderProgram(this->shader);
	this->renderer->render(renderData, viewProjectionMatrix, this->ref, GL_EQUAL);
}

OpenGlRenderer::BackgroundGradientRenderStep::BackgroundGradientRenderStep(BackgroundGradient* gradient, const unsigned int renderLevel, const GLint stencilRef):
	RenderStep(renderLevel, renderLevel),
	gradient(gradient),
	stencilRef(stencilRef)
{
}

void OpenGlRenderer::BackgroundGradientRenderStep::render(RenderData& renderData, const glm::mat4& /*viewProjectionMatrix*/)
{
	renderData.enable(GL_STENCIL_TEST);
	renderData.setStencilMask(0x00);
	renderData.setStencilFunc(GL_GREATER, this->stencilRef, 0xFF);

	this->gradient->render(renderData);
}

OpenGlRenderer::StenciledTiledTextureLayer::StenciledTiledTextureLayer():
	renderLevel(0)
{
}

OpenGlRenderer::StenciledTiledTextureLayer::~StenciledTiledTextureLayer()
{
}

GlslShader OpenGlRenderer::createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const
{
	std::string shaderTypeString = "vertex";
	if (shaderType == GL_FRAGMENT_SHADER)
	{
		shaderTypeString = "fragment";
	}

	this->log.debug().raw(fmt::format("Creating {} shader \"{}\".", shaderTypeString, shaderPath));

	std::string error;
	auto shader = createShaderAutoVersion(resourceCache, shaderPath, shaderType, this->renderData.getGlVersion(), error);

	if (shader.isCreated() == false)
	{
		this->log.error().format("Could not create shader \"%s\": %s", shaderPath.c_str(), error.c_str());
	}
	else if (shader.isCompiled() == false)
	{
		const std::string errorString = shader.getCompileLog();

		if (errorString.empty() == false)
		{
			this->log.error().format("Could not compile shader \"%s\": %s", shaderPath.c_str(), errorString.c_str());
		}
		else
		{
			this->log.error().format("Could not compile shader \"%s\": Unknown reason.", shaderPath.c_str());
		}

		shader.destroy();
	}

	return shader;
}

GlslVertexAndFragmentShader OpenGlRenderer::createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const
{
	GlslVertexAndFragmentShader shader;
	shader.vertex = this->createShader(resourceCache, shaderPath + ".vert", GL_VERTEX_SHADER);
	shader.fragment = this->createShader(resourceCache, shaderPath + ".frag", GL_FRAGMENT_SHADER);

	return shader;
}

bool OpenGlRenderer::setupWindowTextureRendering()
{
	this->windowTextureModel = GlVertexModel(&this->glState);
	this->windowTextureModel.setNumVertices(4);

	this->windowTextureAttributes = this->windowTextureModel.createPackedVertexAttributes(
		GL_STATIC_DRAW,
		TextureQuad::VertexAttribute::generateVertexAttributeDef(
			0,
			1
		));

	const auto screenCoords = makeTriangleStripBox(math::Box<glm::vec2>({-1.0f, -1.0f}, {1.0f, 1.0f}));
	this->windowTextureAttributes->getBuffer().overwriteData(std::move(screenCoords), &this->glState);

	return true;
}

namespace
{

std::vector<TextureQuad::VertexAttribute> makeTriangleStripBox(const math::Box<glm::vec2>& box)
{
	std::vector<TextureQuad::VertexAttribute> coords(4);

	coords[0].setPosition(box.getLowerLeft());
	coords[0].setTextureCoordinate({0.0f, 0.0f});

	coords[1].setPosition(box.getLowerRight());
	coords[1].setTextureCoordinate({1.0f, 0.0f});

	coords[2].setPosition(box.getUpperLeft());
	coords[2].setTextureCoordinate({0.0f, 1.0f});

	coords[3].setPosition(box.getUpperRight());
	coords[3].setTextureCoordinate({1.0f, 1.0f});

	return coords;
}

}

} } }
