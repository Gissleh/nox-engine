/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_RESOURCE_DEFAULTLOADER_H_
#define NOX_RESOURCE_DEFAULTLOADER_H_

#include <nox/app/resource/loader/ILoader.h>

namespace nox { namespace app
{
namespace resource
{

/**
 * Loads all resources as a raw buffer.
 */
class DefualtLoader : public ILoader
{
public:
	DefualtLoader();

	bool useRawFile() const override;
	unsigned int getLoadedResourceSize(const char* rawbuffer, const unsigned int rawSize) const override;
	bool loadResource(const util::Buffer<char> inputBuffer, std::vector<char>& outputBuffer, std::unique_ptr<IExtraData>& extraData) const override;
	const char* getPattern() const override;
};

}
} }

#endif
