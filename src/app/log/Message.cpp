/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/log/Message.h>

namespace nox { namespace app
{
namespace log
{

util::Mask<Message::Level> Message::allLevels()
{
	return util::Mask<Level>
	{
		Level::INFO,
		Level::VERBOSE,
		Level::WARNING,
		Level::ERROR,
		Level::FATAL,
		Level::DEBUG_
	};
}

Message::Message(const Message::Level level, const std::string& message, const std::string& loggerName):
	level(level),
	message(message),
	loggerName(loggerName)
{
}

const std::string& Message::getMessage() const
{
	return this->message;
}

const Message::Level& Message::getLogLevel() const
{
	return this->level;
}

const std::string Message::getLogLevelString() const
{
	switch (this->level)
	{
	case Level::INFO:
		return "info";

	case Level::VERBOSE:
		return "verbose";

	case Level::WARNING:
		return "warning";

	case Level::ERROR:
		return "error";

	case Level::FATAL:
		return "fatal";

	case Level::DEBUG_:
		return "debug";
	}

	return std::string();
}

const std::string& Message::getLoggerName() const
{
	return this->loggerName;
}

}
} }
