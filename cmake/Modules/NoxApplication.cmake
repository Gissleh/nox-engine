function(NoxApplication_Add target sources)
	include(CMakeParseArguments)
	set(ARG_OPTIONS
		NO_ASSET_LIST
	)
	set(ARG_ONEVALUES
		GLES_VERSION_MAJOR
		GLES_VERSION_MINOR
		INPUT_DIR
		APK_PACKAGE_NAME
		APK_SDK_VERSION_TARGET
		APK_SDK_VERSION_MIN
		APK_VERSION_CODE
		APK_VERSION_NAME
		APK_THEME
		APK_ACTIVITY_NAME
		APK_NAME
		APK_SCREEN_ORIENTATION
		APK_MANIFEST
		APK_GRADLE_BUILD
		APK_PROJECTNAME
		APK_SOURCE
	)
	set(ARG_MULTIVALUES
		JAVA_SOURCES
		SHARED_LIBRARIES
		ASSET_DIRS
		APK_RES_SOURCES
		APK_PROJECT_LIBS
	)
	cmake_parse_arguments(NOXAPP "${ARG_OPTIONS}" "${ARG_ONEVALUES}" "${ARG_MULTIVALUES}" ${ARGN})

	if (ANDROID)
		add_library(${target} SHARED ${sources})
	else ()
		add_executable(${target} ${sources})
	endif ()

	target_link_libraries(${target} ${NOX_LINK_TARGET})

	if (NOT NOXAPP_NO_ASSET_LIST)
		if (ANDROID)
			find_host_package(PythonInterp)
		else ()
			find_package(PythonInterp)
		endif ()

		if (NOT PYTHONINTERP_FOUND)
			message(WARNING "Could not find Python. Disabling asset listing.")
			set(NOXAPP_NO_ASSET_LIST ON)
		endif ()
	endif ()

	foreach (ASSET_DIR IN LISTS NOXAPP_ASSET_DIRS)
		get_filename_component(ASSET_DIR_ABS "${ASSET_DIR}" ABSOLUTE)

		file(GLOB_RECURSE ASSET_SOURCE_FILES "${ASSET_DIR}/*")

		set(ASSET_BIN_FILES "")
		foreach (ASSET_SOURCE_FILE IN LISTS ASSET_SOURCE_FILES)
			string(REPLACE "${CMAKE_CURRENT_LIST_DIR}" "${CMAKE_CURRENT_BINARY_DIR}" ASSET_BIN_FILE ${ASSET_SOURCE_FILE})
			list(APPEND ASSET_BIN_FILES "${ASSET_BIN_FILE}")
		endforeach()

		list(APPEND ASSET_COPY_OUTPUTS ${ASSET_BIN_FILES})

		add_custom_command(
			OUTPUT ${ASSET_BIN_FILES}
			DEPENDS ${ASSET_SOURCE_FILES}
			COMMAND "${CMAKE_COMMAND}"
			ARGS -E copy_directory "${ASSET_DIR_ABS}" "${ASSET_DIR}"
			COMMENT "Copying assets from ${ASSET_DIR}"
			VERBATIM
		)

		if (NOT NOXAPP_NO_ASSET_LIST)
			set(ASSET_BIN_DIR "${CMAKE_CURRENT_BINARY_DIR}/${ASSET_DIR}")
			set(ASSET_LIST_FILE "${ASSET_BIN_DIR}/nox-resourcelist.json")
			list(APPEND ASSET_LIST_OUTPUTS "${ASSET_LIST_FILE}")

			add_custom_command(
				OUTPUT "${ASSET_LIST_FILE}"
				DEPENDS "${ASSET_BIN_DIR}"
				COMMAND "${PYTHON_EXECUTABLE}"
				ARGS "${NOX_DIR}/tools/gen_asset_list.py" "${ASSET_DIR}/"
				COMMENT "Creating asset list for ${ASSET_DIR}"
				VERBATIM
			)
		endif ()
	endforeach ()

	add_custom_target("${target}-asset-copy"
		DEPENDS ${ASSET_COPY_OUTPUTS}
	)

	add_custom_target("${target}-asset-list"
		DEPENDS ${ASSET_LIST_OUTPUTS}
	)

	add_dependencies("${target}-asset-list"
		"${target}-asset-copy"
	)

	add_dependencies("${target}"
		"${target}-asset-copy"
		"${target}-asset-list"
	)

	if (ANDROID)
		set(APK_DIR "apk")

		if (NOT NOXAPP_GLES_VERSION_MAJOR)
			set(NOXAPP_GLES_VERSION_MAJOR ${NOX_OPENGL_VERSION_MAJOR})
		endif ()
		if (NOT NOXAPP_GLES_VERSION_MINOR)
			set(NOXAPP_GLES_VERSION_MINOR ${NOX_OPENGL_VERSION_MINOR})
		endif ()
		if (NOT NOXAPP_APK_SDK_VERSION_MIN)
			set(NOXAPP_APK_SDK_VERSION_MIN 10)
		endif ()
		if (NOT NOXAPP_APK_VERSION_CODE)
			set(NOXAPP_APK_VERSION_CODE 1)
		endif ()
		if (NOT NOXAPP_APK_VERSION_NAME)
			set(NOXAPP_APK_VERSION_NAME 1.0)
		endif ()
		if (NOT NOXAPP_APK_THEME)
			set(NOXAPP_APK_THEME "@android:style/Theme.NoTitleBar.Fullscreen")
		endif ()
		if (NOT NOXAPP_APK_PACKAGE_NAME)
			set(NOXAPP_APK_PACKAGE_NAME "com.suttungdigital.nox")
		endif ()
		if (NOT NOXAPP_APK_ACTIVITY_NAME)
			set(NOXAPP_APK_ACTIVITY_NAME "NoxActivity")
		endif ()
		if (NOT NOXAPP_APK_NAME)
			set(NOXAPP_APK_NAME "${target}")
		endif ()
		if (NOT NOXAPP_APK_SCREEN_ORIENTATION)
			set(NOXAPP_APK_SCREEN_ORIENTATION "portrait")
		endif ()
		if (NOT NOXAPP_APK_MANIFEST)
			set(NOXAPP_APK_MANIFEST "${APK_MANIFEST_INPUTFILE}")
		endif ()
		if (NOT NOXAPP_APK_PROJECTNAME)
			set(NOXAPP_APK_PROJECTNAME "${target}")
		endif ()
		if (NOT NOXAPP_INPUT_DIR)
			set(NOXAPP_APK_INPUT_DIR "${NOX_ROOT_DIR}/cmake/Inputs")
		endif ()
		if (NOT NOXAPP_APK_GRADLE_BUILD)
			set(NOXAPP_APK_GRADLE_BUILD "${NOXAPP_INPUT_DIR}/build.gradle")
		endif ()

		# Simpler variable.
		set(PROJECTNAME ${NOXAPP_APK_PROJECTNAME})
		set(PROJECTPATH "${APK_DIR}/${PROJECTNAME}")

		if(NOX_APK_RELEASE)
			set(APK_DEBUGGABLE "false")
		else()
			set(APK_DEBUGGABLE "true")
		endif()

		string(REPLACE "." "/" APK_PACKAGE_DIR ${NOXAPP_APK_PACKAGE_NAME})

		set(APK_GLES_VERSION_HEX "0x000${NOXAPP_GLES_VERSION_MAJOR}000${NOXAPP_GLES_VERSION_MINOR}")
		message(STATUS "APK GLES version is set to ${APK_GLES_VERSION_HEX}")

		# TODO: We should also get all the shared non-system dependencies of the application
		# and add them to NOX_APK_SHARED_LIBS so that they can be loaded.
		if (NOX_BUILD_SHARED)
			list(APPEND NOXAPP_SHARED_LIBRARIES nox-shared)
		endif ()
		list(APPEND NOXAPP_SHARED_LIBRARIES ${target})

		# Get a list of libraries to load in (e.g. "PLCore;PLMath" etc.)
		foreach(SHARED_LIB ${NOXAPP_SHARED_LIBRARIES})
			get_target_property(LIBNAME ${SHARED_LIB} OUTPUT_NAME)
			if (${LIBNAME} STREQUAL "LIBNAME-NOTFOUND")
				set(LIBNAME "${SHARED_LIB}")
			endif ()

			list(APPEND APK_SHARED_LIBRARIES_TO_LOAD "\"${LIBNAME}\",")
		endforeach()

		set(APK_SDK_VERSION_MIN ${NOXAPP_APK_SDK_VERSION_MIN})
		set(APK_SDK_VERSION_TARGET ${NOXAPP_APK_SDK_VERSION_TARGET})
		set(APK_VERSION_CODE ${NOXAPP_APK_VERSION_CODE})
		set(APK_VERSION_NAME ${NOXAPP_APK_VERSION_NAME})
		set(APK_THEME ${NOXAPP_APK_THEME})
		set(APK_ACTIVITY_NAME ${NOXAPP_APK_ACTIVITY_NAME})
		set(APK_NAME ${NOXAPP_APK_NAME})
		set(APK_BUILD_TARGET ${ANDROID_ABI})
		set(APK_SCREEN_ORIENTATION ${NOXAPP_APK_SCREEN_ORIENTATION})
		set(APK_GRADLE_VERSION 2.6)
		set(APK_PACKAGE_NAME ${NOXAPP_APK_PACKAGE_NAME})

		# Create "AndroidManifest.xml"
		configure_file("${NOXAPP_APK_MANIFEST}" ".")

		# Create "res/values/strings.xml"
		#configure_file("${APK_STRINGS_INPUTFILE}" "${PROJECTPATH}/src/main/res/values/strings.xml")

		# Create Java file which is responsible for loading in the required shared libraries (the content of "APK_SHARED_LIBRARIES_TO_LOAD" is used for this)
		configure_file("${APK_JAVAVALUES_INPUTFILE}" "${PROJECTPATH}/src/main/java/com/suttungdigital/android/Values.java")

		# This is the build file for the android build system.
		configure_file("${NOXAPP_APK_GRADLE_BUILD}" "./build.gradle")

		string(REPLACE "${CMAKE_SOURCE_DIR}/" "" CURRENT_RELATIVE_PATH "${CMAKE_CURRENT_LIST_DIR}")

		add_custom_target("${target}-apk" ALL)
		add_dependencies("${target}-apk"
			"${target}"
		)

		if (NOT NOXAPP_APK_SOURCE)
			message(FATAL_ERROR "APK_SOURCE not specified.")
		endif ()

		# Copy sources to APK.
		add_custom_command(
			TARGET "${target}-apk"
			PRE_BUILD
			COMMAND "${CMAKE_COMMAND}"
			ARGS -E copy_directory "${CMAKE_CURRENT_LIST_DIR}/${NOXAPP_APK_SOURCE}" "${APK_DIR}"
			COMMENT "Copying sources from ${NOXAPP_APK_SOURCE} to APK"
			VERBATIM
		)

		# Copy assets to APK.
		foreach (ASSET_DIR IN LISTS NOXAPP_ASSET_DIRS)
			add_custom_command(
				TARGET "${target}-apk"
				PRE_BUILD
				COMMAND "${CMAKE_COMMAND}"
				ARGS -E copy_directory "${CMAKE_CURRENT_BINARY_DIR}/${ASSET_DIR}" "${CMAKE_CURRENT_BINARY_DIR}/${PROJECTPATH}/src/main/assets/${CURRENT_RELATIVE_PATH}/${ASSET_DIR}"
				COMMENT "Copying assets from ${ASSET_DIR} to APK"
				VERBATIM
			)
		endforeach ()

		# Copy the used shared libraries
		foreach(SHARED_LIB ${NOXAPP_SHARED_LIBRARIES})
			set(LIB_COPY_DIR "${PROJECTPATH}/src/main/jniLibs/${APK_BUILD_TARGET}")

			add_custom_command(
				TARGET "${target}-apk"
				PRE_BUILD
				COMMAND "${CMAKE_COMMAND}"
				ARGS -E copy $<TARGET_FILE:${SHARED_LIB}> "${LIB_COPY_DIR}/$<TARGET_FILE_NAME:${SHARED_LIB}>"
				COMMENT "Copying library ${SHARED_LIB} to APK"
				VERBATIM
			)
		endforeach()

		# Copy configured files
		add_custom_command(
			TARGET "${target}-apk"
			PRE_BUILD
			COMMAND "${CMAKE_COMMAND}"
			ARGS -E copy "AndroidManifest.xml" "${PROJECTPATH}/src/main/"
			COMMAND "${CMAKE_COMMAND}"
			ARGS -E copy "build.gradle" "${APK_DIR}/"
			COMMENT "Copying configured files to APK"
			VERBATIM
		)

		# Copy java sources to APK.
		foreach (SOURCE IN LISTS NOXAPP_JAVA_SOURCES)
			add_custom_command(
				TARGET "${target}-apk"
				PRE_BUILD
				COMMAND "${CMAKE_COMMAND}"
				ARGS -E copy_directory "${CMAKE_CURRENT_LIST_DIR}/${SOURCE}" "${PROJECTPATH}/src/main/java"
				COMMENT "Copying java sources from ${SOURCE} to APK"
				VERBATIM
			)
		endforeach ()

		# Copy res sources to APK.
		foreach (SOURCE IN LISTS NOXAPP_APK_RES_SOURCES)
			add_custom_command(
				TARGET "${target}-apk"
				PRE_BUILD
				COMMAND "${CMAKE_COMMAND}"
				ARGS -E copy_directory "${CMAKE_CURRENT_LIST_DIR}/${SOURCE}" "${PROJECTPATH}/src/main/res"
				COMMENT "Copying res sources from ${SOURCE} to APK"
				VERBATIM
			)
		endforeach ()

		# Copy project libraries to APK.
		foreach (LIB IN LISTS NOXAPP_APK_PROJECT_LIBS)
			add_custom_command(
				TARGET "${target}-apk"
				PRE_BUILD
				COMMAND "${CMAKE_COMMAND}"
				ARGS -E copy_directory "${CMAKE_CURRENT_LIST_DIR}/${LIB}" "${APK_DIR}/"
				COMMENT "Copying res sources from ${SOURCE} to APK"
				VERBATIM
			)
		endforeach ()

		# Build the apk.
		if (NOT NOX_APK_RELEASE)
			add_custom_command(
				TARGET "${target}-apk"
				POST_BUILD
				COMMAND "./gradlew"
				ARGS assembleDebug
				WORKING_DIRECTORY "${APK_DIR}"
				COMMENT "Creating debug APK"
				VERBATIM
			)
		else ()
			set(APK_OUTPUT_DIR "${PROJECTNAME}/build/outputs/apk")
			set(APK_UNSIGNED "${APK_OUTPUT_DIR}/${PROJECTNAME}-release-unsigned.apk")
			set(APK_UNALIGNED "${APK_OUTPUT_DIR}/${PROJECTNAME}-release-unaligned.apk")
			set(APK_RELEASE "${APK_OUTPUT_DIR}/${PROJECTNAME}-release.apk")

			add_custom_command(
				TARGET "${target}-apk"
				POST_BUILD
				COMMAND "./gradlew"
				ARGS assembleRelease
				COMMAND jarsigner
				ARGS -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "${NOX_APK_KEYSTORE}" -signedjar "${APK_UNALIGNED}" "${APK_UNSIGNED}" "${NOX_APK_KEYALIAS}"
				COMMAND zipalign
				ARGS -f -v 4 "${APK_UNALIGNED}" "${APK_RELEASE}"
				WORKING_DIRECTORY "${APK_DIR}"
				COMMENT "Creating release APK"
				VERBATIM
			)
		endif ()
	endif ()
endfunction()
