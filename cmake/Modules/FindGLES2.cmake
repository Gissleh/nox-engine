#
# Try to find GLES2 library and include path.
# Once done this will define
#
# GLES2_FOUND
# GLES2_INCLUDE_DIRS
# GLES2_LIBRARIES

find_path(GLES2_INCLUDE_DIR GLES2/gl2.h)
find_library(GLES2_LIBRARY NAMES GLESv2)

if (GLES2_INCLUDE_DIR)
	set(GLES2_INCLUDE_DIRS ${GLES2_INCLUDE_DIR})
endif ()

if (GLES2_LIBRARY)
	set(GLES2_LIBRARIES ${GLES2_LIBRARY})
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(GLES2 DEFAULT_MSG GLES2_LIBRARIES GLES2_INCLUDE_DIRS)
