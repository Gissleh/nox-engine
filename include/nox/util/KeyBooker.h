/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_KEYBOOKER_H_
#define NOX_UTIL_KEYBOOKER_H_

#include <vector>
#include <limits>
#include <stdexcept>
#include <algorithm>

namespace nox { namespace util {

template<class Type>
struct Incrementor
{
	Type operator()(Type& value);
};

template<class Type>
struct Decrementor
{
	Type operator()(Type& value);
};

/**
 * Book unique keys for values.
 * @tparam KeyType The type used for the keys. Usually integral.
 * @tparam ValueType The type used for the values stored.
 */
template<class KeyType, class NextKeyFunctor = Incrementor<KeyType>>
class KeyBooker
{
public:
	KeyBooker(const KeyType& initialKey = KeyType());

	/**
	 * Book a key for a value.
	 * @param value Value to book key for.
	 * @return Unique key for value.
	 */
	KeyType book();

	/**
	 * Unbook a key.
	 * The key will no longer be valid, and can be used by new values.
	 * @param key Key to unbook.
	 */
	void unbook(const KeyType& key);

	/**
	 * Clear all bookings.
	 * None of the keys booked will be valid and unique.
	 */
	void clear();

private:
	KeyType findFreeKey();

	std::vector<KeyType> freeKeyList;
	KeyType nextFreeKey;
	NextKeyFunctor incrementKey;
};

template<class KeyType, class NextKeyFunctor>
KeyBooker<KeyType, NextKeyFunctor>::KeyBooker(const KeyType& initialKey):
	nextFreeKey(initialKey)
{
}

template<class KeyType, class NextKeyFunctor>
KeyType KeyBooker<KeyType, NextKeyFunctor>::book()
{
	const KeyType key = this->findFreeKey();

	return key;
}

template<class KeyType, class NextKeyFunctor>
void KeyBooker<KeyType, NextKeyFunctor>::unbook(const KeyType& key)
{
	const auto keyIt = std::find(this->freeKeyList.begin(), this->freeKeyList.end(), key);
	if (keyIt == this->freeKeyList.end())
	{
		this->freeKeyList.push_back(key);
	}
}

template<class KeyType, class NextKeyFunctor>
inline void KeyBooker<KeyType, NextKeyFunctor>::clear()
{
	this->nextFreeKey = KeyType();

	this->freeKeyList.clear();
}

template<class KeyType, class NextKeyFunctor>
KeyType KeyBooker<KeyType, NextKeyFunctor>::findFreeKey()
{
	KeyType key;

	if (this->freeKeyList.empty() == false)
	{
		key = this->freeKeyList.back();
		this->freeKeyList.pop_back();
	}
	else
	{
		key = this->nextFreeKey;
		this->incrementKey(this->nextFreeKey);
	}

	return key;
}

template<class Type>
inline Type Incrementor<Type>::operator()(Type& value)
{
	if (value >= std::numeric_limits<Type>::max())
	{
		throw std::overflow_error("Can't increment value further as will overflow.");
	}

	++value;
	return value;
}

template<class Type>
inline Type Decrementor<Type>::operator()(Type& value)
{
	if (value <= std::numeric_limits<Type>::lowest())
	{
		throw std::overflow_error("Can't decrement value further as it will overflow.");
	}

	--value;
	return value;
}

} }

#endif
