/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_PROCESS_IMPLEMENTATION_H_
#define NOX_PROCESS_IMPLEMENTATION_H_

#include <nox/common/types.h>

#include <memory>

namespace nox
{

namespace process
{

class Process;

/**
 * Abstract class used for implementing processes.
 * Inherit this and override its method to create your own process implementation.
 */
class Implementation
{
public:
	Implementation();

	virtual ~Implementation();

	/**
	 * Called when process is initialized from the ProcessManager.
	 */
	virtual void onInit() = 0;

	/**
	 * Called each update tick of the ProcessManager.
	 */
	virtual void onUpdate(const Duration& deltaTime) = 0;

	/**
	 * Called when process succeeds its execution.
	 */
	virtual void onSuccess() = 0;

	/**
	 * Called when process fails its execution.
	 */
	virtual void onFail() = 0;

	/**
	 * Called when process execution was aborted.
	 * This happens when user explicitly aborts the process.
	 */
	virtual void onAbort() = 0;

	virtual void onUpdateFinished(const float alpha);

	void onAttachToProcess(Process* process);

protected:
	/**
	 * Set the state of the process to succeeded.
	 * The process will terminate an its successors
	 * will be started.
	 */
	void succeedExecution();

	/**
	 * Set the state of the process to failed.
	 * The process will terminate.
	 */
	void failExecution();

private:
	Process* process;
};

}
}

#endif
