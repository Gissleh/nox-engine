/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_JOINT_H_
#define NOX_LOGIC_PHYSICS_JOINT_H_

#include <nox/logic/actor/Identifier.h>

namespace nox { namespace logic { namespace physics
{

enum class JointType
{
	NONE,
	WELD,
	REVOLUTE,
	PRISMATIC,
	TARGET
};

struct JointDefinition
{
	const JointType type;

	actor::Identifier firstBody;
	actor::Identifier secondBody;
	bool collideBodies;

protected:
	JointDefinition(JointType type, const actor::Identifier& firstBody, const actor::Identifier& secondBody, const bool collideBodies):
		type(type),
		firstBody(firstBody),
		secondBody(secondBody),
		collideBodies(collideBodies)
	{}

	JointDefinition(JointType type, const actor::Identifier& firstBody, const actor::Identifier& secondBody):
		JointDefinition(type, firstBody, secondBody, false)
	{}

	JointDefinition(JointType type):
		JointDefinition(type, actor::Identifier(), actor::Identifier())
	{}
};

struct AnchoredJointDefinition: public JointDefinition
{
	glm::vec2 firstBodyAnchor;
	glm::vec2 secondBodyAnchor;
	float anchorAngle;

protected:
	AnchoredJointDefinition(JointType type):
		JointDefinition(type),
		anchorAngle(0.0f)
	{}
};

struct WeldJointDefinition: public AnchoredJointDefinition
{
	WeldJointDefinition():
		AnchoredJointDefinition(JointType::WELD)
	{}
};

struct RevoluteJointDefinition: public AnchoredJointDefinition
{
	RevoluteJointDefinition():
		AnchoredJointDefinition(JointType::REVOLUTE),
		motor(false),
		motorSpeed(0.0f),
		motorMaxTorque(0.0f)
	{}

	bool motor;
	float motorSpeed;
	float motorMaxTorque;
};

struct PrismaticJointDefinition: public AnchoredJointDefinition
{
	PrismaticJointDefinition():
		AnchoredJointDefinition(JointType::PRISMATIC),
		motor(false),
		enableLimit(false),
		motorSpeed(0.0f),
		maxMotorForce(0.0f),
		lowerTranslation(0.0f),
		upperTranslation(0.0f)
	{}

	bool motor;
	bool enableLimit;
	float motorSpeed;
	float maxMotorForce;
	float lowerTranslation;
	float upperTranslation;
	glm::vec2 axisA;
};

struct TargetJointDefinition: public JointDefinition
{
	TargetJointDefinition(const actor::Identifier& body, const glm::vec2& target, const float maxForce):
		JointDefinition(JointType::TARGET, body, actor::Identifier()),
		target(target),
		maxForce(maxForce)
	{}

	TargetJointDefinition(const actor::Identifier& body, const glm::vec2& target):
		TargetJointDefinition(body, target, 0.0f)
	{}

	TargetJointDefinition(const actor::Identifier& body):
		TargetJointDefinition(body, glm::vec2())
	{}

	TargetJointDefinition():
		TargetJointDefinition(actor::Identifier())
	{}

	glm::vec2 target;
	float maxForce;
};

} } }

#endif
