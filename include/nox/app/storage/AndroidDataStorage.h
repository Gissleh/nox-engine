/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_STORAGE_ANDROIDDATASTORAGE_H_
#define NOX_APP_STORAGE_ANDROIDDATASTORAGE_H_

#include <nox/app/storage/IDataStorage.h>
#include <nox/app/log/Logger.h>

#include <jni.h>

namespace nox { namespace app { namespace storage {

class AndroidDataStorage final: public IDataStorage
{
public:
	AndroidDataStorage();
	AndroidDataStorage(JNIEnv* javaEnv, jobject javaAndroidContext);

	~AndroidDataStorage();

	void setLogger(log::Logger logger);

	bool fileExists(const std::string& filePath) const override;
	std::unique_ptr<std::istream> openReadableFile(const std::string& filePath) override;
	std::unique_ptr<std::ostream> openWritableFile(const std::string& filePath, bool append = false) override;
	std::string readFileContent(const std::string& filePath) override;
	void writeFileContent(const std::string& filePath, const std::string& content, bool append = false) override;
	void removeFile(const std::string& filePath) override;

private:
	class Impl;
	std::unique_ptr<Impl> d;
};

} } }

#endif
