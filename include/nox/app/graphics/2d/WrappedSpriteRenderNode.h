/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_WRAPPEDSPRITERENDERERNODE_H_
#define NOX_APP_GRAPHICS_WRAPPEDSPRITERENDERERNODE_H_

#include "SceneGraphNode.h"
#include <nox/app/graphics/TextureQuad.h>
#include <nox/app/graphics/TextureRenderer.h>
#include <string>

namespace nox { namespace app
{
namespace graphics
{

class WrappedSpriteRenderNode : public SceneGraphNode
{
public:
	WrappedSpriteRenderNode();

	void setRenderLevel(const unsigned int level);
	void setSprite(const std::string& spriteName);
	void setCenterHeight(const float centerHeight);
	void setColor(const glm::vec4& color);

	/**
	 * Sets a wrapping.
	 *
	 * @param localPositions The local positions.
	 */
	void setWrapping(const std::vector<glm::vec2>& localPositions);

	/**
	 * Sets the padding.
	 *
	 * @param leftPadding  The left padding.
	 * @param rightPadding The right padding.
	 */
	void setPadding(const float leftPadding, const float rightPadding);

private:
	struct PointData
	{
		glm::vec2 upper;
		glm::vec2 lower;

		glm::vec2 point;
	};

	/**
	 * Executes the node enter action.
	 *
	 * @param [in,out] renderData  Information describing the render.
	 * @param [in,out] modelMatrix The model matrix.
	 */
	void onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix) override;

	void onAttachToRenderer(IRenderer* renderer) override;

	void onDetachedFromRenderer() override;

	/** Adds the padding to local points if specified. */
	void addPaddingToLocalPositions();

	unsigned int levelID;

	std::string spriteName;
	TextureQuad sprite;
	glm::vec4 color;
	float centerHeight;

	bool needUpdate;

	TextureRenderer::DataHandle spriteDataHandle;
	glm::mat4 previousMatrix;

	std::vector<TextureQuad::RenderQuad> textureQuads;
	std::vector<glm::vec2> localWrapPositions;

	float leftPadding;
	float rightPadding;
};

}
} }

#endif
