/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_RENDERCAMERA_H_
#define NOX_APP_GRAPHICS_RENDERCAMERA_H_

#include <glm/glm.hpp>
#include <nox/util/math/Box.h>

namespace nox { namespace app
{
namespace graphics
{

class Camera
{
public:
	Camera(const glm::uvec2& size);

	void setSize(const glm::uvec2& size);
    
	const glm::mat4x4& getViewProjectionMatrix() const;
	const glm::mat4x4& getViewMatrix() const;
    
	void setPosition(const glm::vec2& position);
	glm::vec2 getPosition() const;
    
	void setRotation(float rotation);
	float getRotation() const;
    
	void setScale(const glm::vec2& scale);
	glm::vec2 getScale() const;
    
	math::Box<glm::vec2> getBoundingAABB() const;
	glm::vec2 getSize() const;

private:
	void updateCameraMatrix();

	glm::uvec2 cameraSize;

	glm::vec2 cameraPosition;
	float cameraRotation;
	glm::vec2 cameraScale;

	glm::mat4x4 viewProjectionMatrix;
	glm::mat4x4 viewMatrix;
	glm::mat4x4 projectionMatrix;
};

}
} }

#endif
