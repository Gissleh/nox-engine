/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_SCENEGRAPHNODE_H_
#define NOX_APP_GRAPHICS_SCENEGRAPHNODE_H_

#include <memory>
#include <vector>
#include <glm/glm.hpp>

namespace nox { namespace app
{
namespace graphics
{

class RenderData;
class TextureRenderer;
class IRenderer;

class SceneGraphNode
{
public:
	SceneGraphNode();
	virtual ~SceneGraphNode();

	void addChild(const std::shared_ptr<SceneGraphNode>& child);
	void removeChild(const std::shared_ptr<SceneGraphNode>& childToBeRemoved);
	void removeChildren();

	void setCurrentRenderer(IRenderer* renderer);
	void removeCurrentRenderer();

	SceneGraphNode* getParent();
	bool hasNodeInTree(const std::shared_ptr<SceneGraphNode>& node) const;
    
	/**
	 *  Recursive function for doing the sorting of the objects into the array
	 *  and modifying the array along the way.
	 */
	void onTraverse(TextureRenderer& renderData, glm::mat4x4& modelMatrix);

protected:
	virtual void onDetachedFromParent();
	IRenderer* getCurrentRenderer();
    
private:
	/**
	 * Called when this or a parent node is attached to a new renderer
	 * @param renderer Renderer attached to.
	 */
	virtual void onAttachToRenderer(IRenderer* renderer);


	virtual void onDetachedFromRenderer();

	/**
	 * Called when the graph traversal enters this node.
	 * @param renderReadyObject Container for the sprite coordinate array.
	 * @param modelMatrix The current translation matrix.
	 */
	virtual void onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix) = 0;

	/**
	 * Called when the graph traversal leaves this node (done with its children).
	 * @param renderReadyObject Container for the sprite coordinate array.
	 * @param modelMatrix The current translation matrix.
	 */
	virtual void onNodeLeave(TextureRenderer& renderData, glm::mat4x4& modelMatrix);

	std::vector<std::shared_ptr<SceneGraphNode>> children;

	SceneGraphNode* parent;

	/** The renderer this node (or parents) is attached to. nullptr if not attached. */
	IRenderer* currentRenderer;
};

}
} }

#endif
