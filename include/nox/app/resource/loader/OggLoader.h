/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_OGGLOADER_H_
#define NOX_APP_RESOURCE_OGGLOADER_H_

#include <nox/app/resource/loader/ILoader.h>
#include <nox/common/api.h>

#include <memory>

namespace nox { namespace app
{
namespace resource
{

class SoundExtraData;

/**
 * Loader for loading ogg files into memory.
 */
class NOX_API OggLoader : public ILoader
{
public:

	/**
	* This function makes sure you don't use the raw file.
	* @return false
	*/
	bool useRawFile() const override;

	/**
	* This method get the size of a audio file.
	* @param rawBuffer The raw buffer to the resource.
	* @param rawSize The raw size of the resource.
	* @return resouce size.
	*/
	unsigned int getLoadedResourceSize(const char* rawBuffer, const unsigned int rawSize) const override;

	/**
	* This method decompresses the resource by using the parseOgg method.
	* @param rawBuffer
	* @param rawSize
	* @param handle
	* @return false if parseOgg retruns false else it will return true.
	*/
	bool loadResource(const util::Buffer<char> inputBuffer, std::vector<char>& outputBuffer, std::unique_ptr<IExtraData>& extraData) const override;

	/**
	* Gets the ogg file name by checking its name with a wildcard match.
	* @return *.ogg
	*/
	const char* getPattern() const override;

private:

	/**
	* This method will decompress an ogg memory buffer by using vorbis API. It decompresses the ogg
	* stream into a PCM buffer.
	* @param oggStream data ptr.
	* @param length data size.
	* @param handle to the resource.
	* @return false if handle's size is not equal to byte size else return true.
	*/
	std::unique_ptr<SoundExtraData> parseOgg(const util::Buffer<char> inputBuffer, std::vector<char>& outputBuffer) const;
};

}
} }

#endif
