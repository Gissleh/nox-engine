package com.suttungdigital.nox;

import org.libsdl.app.SDLActivity;
import com.suttungdigital.android.Values;

public class NoxActivity extends SDLActivity {
    @Override
    protected String[] getLibraries() {
	    return Values.LIBRARIES;
    }
}
