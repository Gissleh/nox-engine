import argparse
import os
from collections import namedtuple
import json

argparser = argparse.ArgumentParser(description='Generate JSON list of assets in an asset direectory. Can be used by a resource Provider.')
argparser.add_argument('assetdir', help='Directory to list assets in, and put list file.')

args = argparser.parse_args()

assetdir = args.assetdir

assetlist = {}
current_asset_parent = assetlist
dirs_to_scan = []

Dir = namedtuple('Dir', 'path assetlist')

dirs_to_scan.append(Dir(assetdir, assetlist))

while (dirs_to_scan):
    current_dir = dirs_to_scan.pop();

    subfiles = os.listdir(current_dir.path);

    for file in subfiles:
        filepath = os.path.join(current_dir.path, file)
        local_filepath = filepath.replace(assetdir, '')
        local_filepath = local_filepath.replace('\\', '/')

        if os.path.isfile(filepath):
            current_dir.assetlist[local_filepath] = 'file'
        elif os.path.isdir(filepath):
            dir_assetlist = {}
            current_dir.assetlist[local_filepath] = dir_assetlist
            dirs_to_scan.append(Dir(filepath, dir_assetlist))

assetlist_json = json.dumps(assetlist, indent=4)

listfile = open(os.path.join(assetdir, 'nox-resourcelist.json'), 'w')
listfile.write(assetlist_json)
