add_executable(test-logic-context
	TestLogicContext.cpp
)

target_link_libraries(test-logic-context
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(test-logic-context PROPERTIES
    FOLDER ${NOX_TEST_FOLDER}
)

add_test(
	NAME test-logic-context
	COMMAND test-logic-context
)
