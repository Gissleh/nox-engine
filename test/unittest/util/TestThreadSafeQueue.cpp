#include <gtest/gtest.h>
#include <future>

#include <nox/util/thread/ThreadSafeQueue.h>

TEST(TestThreadSafeQueue, PushPop)
{
	nox::thread::ThreadSafeQueue<int> queue;

	int expectedResult = 0;

	for (int i = 0; i < 1000; i++)
	{
		queue.push(i);
		expectedResult += i;
	}

	int result = 0;

	while (queue.isEmpty() == false)
	{
		result += queue.pop();
	}

	EXPECT_EQ(expectedResult, result);
}

TEST(TestThreadSafeQueue, Size)
{
	nox::thread::ThreadSafeQueue<int> queue;

	for (int i = 0; i < 10000; i++)
	{
		queue.push(i);
	}

	EXPECT_EQ(10000, queue.getSize());
}

TEST(TestThreadSafeQueue, Empty)
{
	nox::thread::ThreadSafeQueue<int> queue;

	EXPECT_TRUE(queue.isEmpty());

	queue.push(1);

	EXPECT_FALSE(queue.isEmpty());

	queue.pop();

	EXPECT_TRUE(queue.isEmpty());
}

TEST(TestThreadSafeQueue, Clear)
{
	nox::thread::ThreadSafeQueue<int> queue;

	for (int i = 0; i < 10000; i++)
	{
		queue.push(i);
	}

	queue.clear();

	EXPECT_TRUE(queue.isEmpty());
}

TEST(TestThreadSafeQueue, AsynchronousPushPop)
{
	nox::thread::ThreadSafeQueue<int> queue;

	const unsigned int numOperations = 100000;
	bool pushing = true;

	std::future<int> futureSumPushed = std::async(
		std::launch::async,
		[numOperations, &queue, &pushing]()
		{
			int sum = 0;

			for (unsigned int i = 0; i < numOperations; i++)
			{
				int value = static_cast<int>(i + 1);
				queue.push(value);
				sum += value;
			}

			pushing = false;
			return sum;
		}
	);

	std::future<int> futureSumPopped = std::async(
		std::launch::async,
		[numOperations, &queue, &pushing]()
		{
			int sum = 0;

			while (pushing == true || queue.isEmpty() == false)
			{
				sum += queue.pop(0);
			}

			return sum;
		}
	);

	const int sumPushed = futureSumPushed.get();
	const int sumPopped = futureSumPopped.get();

	EXPECT_EQ(sumPushed, sumPopped);
}
