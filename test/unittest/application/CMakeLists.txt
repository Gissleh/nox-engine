add_executable(test-application
	TestApplication.cpp
)

target_link_libraries(test-application
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(test-application PROPERTIES
    FOLDER ${NOX_TEST_FOLDER}
)

add_test(
	NAME test-application
	COMMAND test-application
)

add_subdirectory(log)
